﻿using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class LinkAnswerResultDataModel : BaseNotify
    {
        #region Fields
        private int _testResultId;
        private int _questionId;
        private int _answerId;
        private TestResultsDataModel _testResults = new TestResultsDataModel();
        private AnswerDataModel _answer = new AnswerDataModel();
        private QuestionDataModel _question = new QuestionDataModel();
        #endregion

        #region Properties
        public int TestResultId
        {
            get { return _testResultId; }
            set { _testResultId = value; OnPropertyChanged("TestResultId"); }
        }
        public int QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; OnPropertyChanged("QuestionId"); }
        }
        public int AnswerId
        {
            get { return _answerId; }
            set { _answerId = value; OnPropertyChanged("AnswerId"); }
        }

        public virtual TestResultsDataModel TestResults
        {
            get { return _testResults; }
            set { _testResults = value; }
        }

        public virtual AnswerDataModel Answer
        {
            get { return _answer; }
            set { _answer = value; }
        }

        public virtual QuestionDataModel Question
        {
            get { return _question; }
            set { _question = value; }
        }
        #endregion
    }
}