﻿using System;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
	public class AssignedTestsModel: BaseNotify
    {
        #region Fields
        private int _testId;
        private string _subjectName;
        private string _testName;
        private DateTime _assignedDate;
        private bool _testIsEndEdit;
        private int _questionCount;
        private TimeSpan? _testSetTime;
        private string _testType;
        #endregion

        #region Properties
        public int TestId
		{
			get { return _testId; }
			set { _testId = value; OnPropertyChanged("TestId");}
		}

		public string SubjectName
		{
			get { return _subjectName; }
			set { _subjectName = value; OnPropertyChanged("SubjectName");}
		}

		public string TestName
		{
			get { return _testName; }
			set { _testName = value; OnPropertyChanged("TestName");}
		}

		public DateTime AssignedDate
		{
			get { return _assignedDate; }
			set { _assignedDate = value; OnPropertyChanged("AssignedDate"); }
		}

		public int QuestionCount
		{
			get { return _questionCount; }
			set { _questionCount = value; OnPropertyChanged("QuestionCount");}
		}

		public bool TestIsEndEdit
		{
			get { return _testIsEndEdit; }
			set { _testIsEndEdit = value; OnPropertyChanged("TesttIsEndEdit");}
		}

		public TimeSpan? TestSetTime
		{
			get { return _testSetTime; }
			set { _testSetTime = value; OnPropertyChanged("TestSetTime");}
		}

		public string TestType
		{
			get { return _testType; }
			set { _testType = value; OnPropertyChanged("TestType");}
        }
        #endregion
    }
}
