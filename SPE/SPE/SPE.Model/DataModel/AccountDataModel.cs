﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel.Helpers;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class AccountDataModel : BaseNotify, IEquatable<AccountDataModel>
    {
        #region Fields
        private int _accountId;
        private string _login;
        private string _password;
        private string _name;
        private string _middleName;
        private string _lastName;
        private DateTime _birthday;
        private bool _isActive;
        private bool _isEnable;

        private ICollection<LinkAccountTestDataModel> _linkAccountTest = new HashSet<LinkAccountTestDataModel>();
        private ICollection<LinkAccountTestDataModel> _linkAccountTest_1 = new HashSet<LinkAccountTestDataModel>();
        private ICollection<TestResultsDataModel> _testResults = new HashSet<TestResultsDataModel>();
        private ICollection<RoleDataModel> _role = new HashSet<RoleDataModel>();
        private ICollection<SubjectDataModel> _subject = new HashSet<SubjectDataModel>();
        #endregion

        #region Properties
        public virtual ICollection<RoleDataModel> Role
        {
            get { return _role; }
            set { _role.ToList().AddRange(value); }
        }

        public virtual ICollection<SubjectDataModel> Subject
        {
            get { return _subject; }
            set { _subject.ToList().AddRange(value); }
        }

        public virtual ICollection<LinkAccountTestDataModel> LinkAccountTest
        {
            get { return _linkAccountTest; }
            set { _linkAccountTest.ToList().AddRange(value); }
        }

        public virtual ICollection<LinkAccountTestDataModel> LinkAccountTest_1
        {
            get { return _linkAccountTest_1; }
            set { _linkAccountTest_1.ToList().AddRange(value); }
        }

        public virtual ICollection<TestResultsDataModel> TestResults
        {
            get { return _testResults; }
            set { _testResults.ToList().AddRange(value); }
        }

        public int AccountId
        {
            get { return _accountId; }
            set { _accountId = value; OnPropertyChanged("AccountId"); }
        }

        public string Login
        {
            get { return _login; }
            set { _login = value; OnPropertyChanged("Login"); }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged("Password"); }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }

        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; OnPropertyChanged("MiddleName"); }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; OnPropertyChanged("LastName"); }
        }

        public DateTime Birthday
        {
            get { return _birthday; }
            set { _birthday = value; OnPropertyChanged("Birthday"); }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; OnPropertyChanged("IsActive"); }
        }

        public bool IsEnable
        {
            get { return _isEnable; }
            set { _isEnable = value; OnPropertyChanged("IsEnable"); }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format(RS.AccountName, Name, MiddleName, LastName);
        }

        public bool Equals(AccountDataModel other)
        {
            return _name.Equals(other.Name) &&
                _middleName.Equals(other.MiddleName) &&
                _lastName.Equals(other.LastName) &&
                _login.Equals(other.Login) &&
                _isEnable.Equals(other.IsEnable) &&
                _birthday.Equals(other._birthday) &&
                CheckRoles(Role, other.Role);
        }

        private static bool CheckRoles(ICollection<RoleDataModel> old, ICollection<RoleDataModel> other)
        {
            if (old.Count != other.Count) return false;
            if (old.Count == 0) return true;
            for (int i = 0; i < old.Count(); i++)
            {
                if (!old.ToArray()[i].Equals(other.ToArray()[i])) return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            var temp = obj as AccountDataModel;
            return (temp != null) && Equals(temp);
        }
        #endregion

        #region SortHelper
        public static IComparer<AccountDataModel> SortModeration()
        {
            return new SortRoleModerationHelper();
        }

        public static IComparer<AccountDataModel> SortName()
        {
            return new SortAccountNameHelper();
        }
        #endregion
    }
}