﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class SubjectDataModel : BaseNotify
    {
        #region Fields
        private int _subjectId;
        private string _subjectName = String.Empty;
        private string _subjectDesc = String.Empty;
        private ICollection<AccountDataModel> _account = new HashSet<AccountDataModel>();
        private ICollection<TestDataModel> _test = new HashSet<TestDataModel>();
        private ICollection<ThemeDataModel> _theme = new HashSet<ThemeDataModel>();
        #endregion

        #region Properties
        public int SubjectId
        {
            get { return _subjectId; }
            set { _subjectId = value; OnPropertyChanged("SubjectId"); }
        }
        public string SubjectName
        {
            get { return _subjectName; }
            set { _subjectName = value; OnPropertyChanged("SubjectName"); }
        }
        public string SubjectDesc
        {
            get { return _subjectDesc; }
            set { _subjectDesc = value; OnPropertyChanged("SubjectDesc"); }
        }

        public virtual ICollection<AccountDataModel> Account
        {
            get { return _account; }
            set { _account.ToList().AddRange(value); }
        }

        public virtual ICollection<TestDataModel> Test
        {
            get { return _test; }
            set { _test.ToList().AddRange(value); }
        }

        public virtual ICollection<ThemeDataModel> Theme
        {
            get { return _theme; }
            set { _theme.ToList().AddRange(value); }
        }
        #endregion
    }
}