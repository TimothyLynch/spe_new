﻿using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel.Common;
using System;

namespace SPE.Model.DataModel
{
    public class AnswerDataModel : BaseNotify
    {
        #region Fields
        private int _answerId;
        private string _answerText = String.Empty;
        private string _answerDesc = String.Empty;
        private int _typeAnswerValue;
        private int _questionId;
        private bool _isChecked;
        private ICollection<LinkAnswerResultDataModel> _linkAnswerResult = new HashSet<LinkAnswerResultDataModel>();
        private QuestionDataModel _question = new QuestionDataModel();
        #endregion

        #region Properties
        public int AnswerId
        {
            get { return _answerId; }
            set { _answerId = value; OnPropertyChanged("AnswerId"); }
        }
        public string AnswerText
        {
            get { return _answerText; }
            set { _answerText = value; OnPropertyChanged("AnswerText"); }
        }
        public string AnswerDesc
        {
            get { return _answerDesc; }
            set { _answerDesc = value; OnPropertyChanged("AnswerDesc"); }
        }
        public int TypeAnswerValue
        {
            get { return _typeAnswerValue; }
            set { _typeAnswerValue = value; OnPropertyChanged("TypeAnswerValue"); }
        }
        public int QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; OnPropertyChanged("QuestionId"); }
        }
        public bool IsChecked
        {
            get { return _isChecked; }
            set { _isChecked = value; OnPropertyChanged("IsChecked"); }
        }

        public virtual ICollection<LinkAnswerResultDataModel> LinkAnswerResult
        {
            get { return _linkAnswerResult; }
            set { _linkAnswerResult.ToList().AddRange(value); }
        }

        public virtual QuestionDataModel Question
        {
            get { return _question; }
            set { _question = value; }
        }
        #endregion
    }
}