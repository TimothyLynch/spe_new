﻿using System;
using System.Collections.Generic;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class RoleDataModel : BaseNotify, IEquatable<RoleDataModel>
    {
        #region Fields
        private int _roleId;
        private string _name;
        private string _description;
        private bool _moderation;
        private ICollection<AccountDataModel> _account = new HashSet<AccountDataModel>();
        #endregion 

        #region Properties
        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; OnPropertyChanged("RoleId"); }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged("Description"); }
        }

        public bool Moderation
        {
            get { return _moderation; }
            set { _moderation = value; OnPropertyChanged("Moderation"); }
        }

        public virtual ICollection<AccountDataModel> Account
        {
            get { return _account; }
            set { _account = value; }
        }
        #endregion 

        #region Methods
        public bool Equals(RoleDataModel other)
        {
            return _name.Equals(other.Name) && _moderation == other._moderation;
        }

        public override bool Equals(object obj)
        {
            var temp = obj as RoleDataModel;
            return (temp != null) && Equals(temp);
        }
        #endregion
    }
}