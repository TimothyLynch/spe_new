﻿using System.Collections.Generic;
using SPE.Model.DataModel.Common;
using System;

namespace SPE.Model.DataModel
{
    public class QuestionDataModel : BaseNotify
    {
        #region Fields
        private int _idQuestion;
        private string _textQuestion = String.Empty;
        private string _descQuestion = String.Empty;
        private int _typeQuestion;
        private ICollection<LinkAnswerResultDataModel> _linkAnswerResult = new HashSet<LinkAnswerResultDataModel>();
        private ICollection<TestDataModel> _test = new HashSet<TestDataModel>();
        private ICollection<ThemeDataModel> _theme = new HashSet<ThemeDataModel>();
        private ICollection<AnswerDataModel> _answer = new HashSet<AnswerDataModel>();
        private string _displayName = string.Empty;
        #endregion
        
        #region Properties
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; OnPropertyChanged("DisplayName"); }
        }

        public int IdQuestion
        {
            get { return _idQuestion; }
            set { _idQuestion = value; OnPropertyChanged("IdQuestion"); }
        }
        public string TextQuestion
        {
            get { return _textQuestion; }
            set { _textQuestion = value; OnPropertyChanged("TextQuestion"); }
        }
        public string DescQuestion
        {
            get { return _descQuestion; }
            set { _descQuestion = value; OnPropertyChanged("DescQuestion"); }
        }

        public int TypeQuestion 
        {
            get { return _typeQuestion; }
            set { _typeQuestion = value; OnPropertyChanged("TypeQuestion"); }
        }

        public virtual ICollection<LinkAnswerResultDataModel> LinkAnswerResult
        {
            get { return _linkAnswerResult; }
            set { _linkAnswerResult = value; }
        }

        public virtual ICollection<TestDataModel> Test
        {
            get { return _test; }
            set { _test = value; }
        }

        public virtual ICollection<ThemeDataModel> Theme
        {
            get { return _theme; }
            set { _theme = value; }
        }

        public virtual ICollection<AnswerDataModel> Answer
        {
            get { return _answer; }
            set { _answer = value; }
        }
        #endregion
    }
}