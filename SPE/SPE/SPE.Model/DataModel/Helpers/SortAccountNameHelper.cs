﻿using System;
using System.Collections.Generic;

namespace SPE.Model.DataModel.Helpers
{
    internal class SortAccountNameHelper : IComparer<AccountDataModel>
    {
        public int Compare(AccountDataModel x, AccountDataModel y)
        {
            return String.CompareOrdinal(x.Name, y.Name);
        }
    }
}