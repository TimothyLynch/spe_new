﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SPE.Model.DataModel.Helpers
{
    internal class SortRoleModerationHelper : IComparer<AccountDataModel>
    {
        public int Compare(AccountDataModel x, AccountDataModel y)
        {
            var xList = new List<RoleDataModel>();
            var yList = new List<RoleDataModel>();
            xList.AddRange(x.Role);
            yList.AddRange(y.Role);
            if (xList.Any(a => a.Moderation == false) && yList.FirstOrDefault(a => a.Moderation == false) == null) return -1;
            if (xList.FirstOrDefault(a => a.Moderation == false) == null && yList.Any(a => a.Moderation == false)) return 1;
            if (xList.Any(a => a.Moderation == false) && yList.Any(a => a.Moderation == false)) return String.CompareOrdinal(x.Login,y.Login);
            if (xList.FirstOrDefault(a => a.Moderation == false) == null && yList.FirstOrDefault(a => a.Moderation == false) == null) return String.CompareOrdinal(x.Login, y.Login);

            return 0;
        }
    }
}