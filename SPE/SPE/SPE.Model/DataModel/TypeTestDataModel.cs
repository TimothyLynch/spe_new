﻿using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class TypeTestDataModel : BaseNotify
    {
        #region Fields

        private int _typeTestId;
        private string _typeTestName;
        private string _typeTestDesc;
        private ICollection<TestDataModel> _test = new HashSet<TestDataModel>();

        #endregion

        #region Properties

        public int TypeTestId
        {
            get { return _typeTestId; }
            set
            {
                _typeTestId = value;
                OnPropertyChanged("TypeTestId");
            }
        }

        public string TypeTestName
        {
            get { return _typeTestName; }
            set
            {
                _typeTestName = value;
                OnPropertyChanged("TypeTestName");
            }
        }

        public string TypeTestDesc
        {
            get { return _typeTestDesc; }
            set
            {
                _typeTestDesc = value;
                OnPropertyChanged("TypeTestDesc");
            }
        }

        public virtual ICollection<TestDataModel> Test
        {
            get { return _test; }
            set { _test.ToList().AddRange(value); }
        }

        #endregion
    }
}