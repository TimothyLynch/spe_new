﻿using System.Collections.Generic;
using System;
using System.Linq;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class TestDataModel : BaseNotify
    {
        #region Fields
        private int _testId;
        private string _testName;
        private string _testDesc;
        private bool _testIsEndEdit;
        private int _testCountViewQuestion;
        private TimeSpan? _testSetTime = new TimeSpan(0, 60, 0);
        private int _subjectId;
        private ICollection<TestResultsDataModel> _testResult = new HashSet<TestResultsDataModel>();
        private ICollection<QuestionDataModel> _question = new HashSet<QuestionDataModel>();
        private ICollection<LinkAccountTestDataModel> _linkAccountTest = new HashSet<LinkAccountTestDataModel>();
        private ICollection<TypeTestDataModel> _typeTest = new HashSet<TypeTestDataModel>();
        private SubjectDataModel _subject = new SubjectDataModel();
        #endregion

        #region Properties
        public int TestId
        {
            get { return _testId; }
            set { _testId = value; OnPropertyChanged("TestId"); }
        }
        public string TestName
        {
            get { return _testName; }
            set { _testName = value; OnPropertyChanged("TestName"); }
        }
        public string TestDesc
        {
            get { return _testDesc; }
            set { _testDesc = value; OnPropertyChanged("TestDesc"); }
        }
        public bool TestIsEndEdit
        {
            get { return _testIsEndEdit; }
            set { _testIsEndEdit = value; OnPropertyChanged("TestIsEndEdit"); }
        }
        public int TestCountViewQuestion
        {
            get { return _testCountViewQuestion; }
            set { _testCountViewQuestion = value; OnPropertyChanged("TestCountViewQuestion"); }
        }
        public TimeSpan? TestSetTime
        {
            get { return _testSetTime; }
            set { _testSetTime = value; OnPropertyChanged("TestSetTime"); }
        }
        public int SubjectId
        {
            get { return _subjectId; }
            set { _subjectId = value; OnPropertyChanged("SubjectId"); }
        }

        public virtual ICollection<TestResultsDataModel> TestResult 
        { 
            get { return _testResult; }
            set { _testResult.ToList().AddRange(value); }
        }

        public virtual ICollection<QuestionDataModel> Question 
        { 
            get { return _question; }
            set { _question.ToList().AddRange(value); }
        }

        public virtual ICollection<LinkAccountTestDataModel> LinkAccountTest
        { 
            get { return _linkAccountTest; }
            set { _linkAccountTest.ToList().AddRange(value); }
        }

        public virtual ICollection<TypeTestDataModel> TypeTest 
        { 
            get { return _typeTest; }
            set { _typeTest.ToList().AddRange(value); }
        }

        public virtual SubjectDataModel Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        #endregion
    }
}