﻿using System.ComponentModel;

namespace SPE.Model.DataModel.Common
{
    public class BaseNotify : INotifyPropertyChanged
    {
        #region Implement INotyfyPropertyChanged members
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion Implement INotyfyPropertyChanged members
    }
}