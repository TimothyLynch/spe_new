﻿using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel.Common;
using System;

namespace SPE.Model.DataModel
{
    public class ThemeDataModel : BaseNotify
    {
        #region Fields
        private int _themeId;
        private string _themeName = String.Empty;
        private string _themeDesc = String.Empty;
        private ICollection<QuestionDataModel> _question = new HashSet<QuestionDataModel>();
        private ICollection<SubjectDataModel> _subject = new HashSet<SubjectDataModel>();
        #endregion

        #region Properties
        public int ThemeId
        {
            get { return _themeId; }
            set
            {
                _themeId = value;
                OnPropertyChanged("ThemeId");
            }
        }

        public string ThemeName
        {
            get { return _themeName; }
            set
            {
                _themeName = value;
                OnPropertyChanged("ThemeName");
            }
        }

        public string ThemeDesc
        {
            get { return _themeDesc; }
            set
            {
                _themeDesc = value;
                OnPropertyChanged("ThemeDesc");
            }
        }

        public virtual ICollection<QuestionDataModel> Question
        {
            get { return _question; }
            set { _question.ToList().AddRange(value); }
        }

        public virtual ICollection<SubjectDataModel> Subject
        {
            get { return _subject; }
            set { _subject.ToList().AddRange(value); }
        }
        #endregion
    }
}