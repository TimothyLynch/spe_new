﻿using System;

namespace SPE.Model.DataModel
{
	public class StudentStatisticModel
    {
        #region Properties
        public int TestId { get; set; }
		public string SubjectName { get; set; }
		public string TestName { get; set; }
		public DateTime? PassingDate { get; set; }
		public int QuestionCount { get; set; }
		public int? CountBadQuestions { get; set; }
		public TimeSpan TimeTest { get; set; }
        public TimeSpan TimeToTest { get; set; }
		public int StatusTest { get; set; }
		public string TestType { get; set; }
        #endregion
    }
}
