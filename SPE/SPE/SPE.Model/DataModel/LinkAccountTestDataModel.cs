﻿using System;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class LinkAccountTestDataModel : BaseNotify
    {
        #region Fields
        private int _assignedTestId;
        private int _testId;
        private int _accountId;
        private DateTime _assignedDate = DateTime.Now;
        private int _whoAssignedAccountId;
        private AccountDataModel _account = new AccountDataModel();
        private AccountDataModel _account1 = new AccountDataModel();
        private TestDataModel _test  = new TestDataModel();
        #endregion

        #region Properties
        public int AssignedTestId
        {
            get { return _assignedTestId; }
            set { _assignedTestId = value; OnPropertyChanged("AssignedTestId"); }
        }
        public int TestId
        {
            get { return _testId; }
            set { _testId = value; OnPropertyChanged("TestId"); }
        }
        public int AccountId
        {
            get { return _accountId; }
            set { _accountId = value; OnPropertyChanged("AccountId"); }
        }
        public DateTime AssignedDate
        {
            get { return _assignedDate; }
            set { _assignedDate = value; OnPropertyChanged("AssignedDate"); }
        }
        public int WhoAssignedAccountId
        {
            get { return _whoAssignedAccountId; }
            set { _whoAssignedAccountId = value; OnPropertyChanged("WhoAssignedAccountId"); }
        }

        public virtual AccountDataModel Account 
        { 
            get { return _account; }
            set { _account = value; } 
        }

        public virtual AccountDataModel Account_1 
        { 
            get { return _account1; }
            set { _account1 = value; } 
        }

        public virtual TestDataModel Test 
        { 
            get { return _test; } 
            set { _test = value; }
        }
        #endregion
    }
}