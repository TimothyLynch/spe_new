﻿using System.Collections.Generic;
using System;
using SPE.Model.DataModel.Common;

namespace SPE.Model.DataModel
{
    public class TestResultsDataModel : BaseNotify
    {
        #region Fields
        private int _testResultId;
        private int _testId;
        private int _accountId;
        private int _typeTestId;
        private bool _testResult1;
        private DateTime _testDateTime;
        private int _testCountQuestion;
        private int _countBadQuestion;
        private TimeSpan _timeToTest;
        private TimeSpan _timeTest;
        private int _statusTest;
        #endregion

        #region Properties
        public int TestResultId
        {
            get { return _testResultId; }
            set { _testResultId = value; OnPropertyChanged("TestResultId"); }
        }
        public int TestId
        {
            get { return _testId; }
            set { _testId = value; OnPropertyChanged("TestId"); }
        }
        public int AccountId
        {
            get { return _accountId; }
            set { _accountId = value; OnPropertyChanged("AccountId"); }
        }
        public int TypeTestId
        {
            get { return _typeTestId; }
            set { _typeTestId = value; OnPropertyChanged("TypeTestId"); }
        }
        public bool TestResult1
        {
            get { return _testResult1; }
            set { _testResult1 = value; OnPropertyChanged("TestResult"); }
        }
        public DateTime TestDateTime
        {
            get { return _testDateTime; }
            set { _testDateTime = value; OnPropertyChanged("TestDateTime"); }
        }
        public int TestCountQuestion
        {
            get { return _testCountQuestion; }
            set { _testCountQuestion = value; OnPropertyChanged("TestCountQuestion"); }
        }
        public int CountBadQuestion
        {
            get { return _countBadQuestion; }
            set { _countBadQuestion = value; OnPropertyChanged("CountBadQuestion"); }
        }
        public TimeSpan TimeToTest
        {
            get { return _timeToTest; }
            set { _timeToTest = value; OnPropertyChanged("TimeToTest"); }
        }
        public TimeSpan TimeTest
        {
            get { return _timeTest; }
            set { _timeTest = value; OnPropertyChanged("TimeTest"); }
        }
        public int StatusTest
        {
            get { return _statusTest; }
            set { _statusTest = value; OnPropertyChanged("StatusTest"); }
        }
        public virtual AccountDataModel Account { get; set; }
        public virtual ICollection<LinkAnswerResultDataModel> LinkAnswerResult { get; set; }
        public virtual TestDataModel Test { get; set; }
        #endregion
    }
}