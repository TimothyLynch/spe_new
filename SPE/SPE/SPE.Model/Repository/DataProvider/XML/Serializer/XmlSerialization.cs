﻿using System.Collections;
using System.IO;
using System.Threading;
using System.Xml.Serialization;

namespace SPE.Model.Repository.DataProvider.XML.Serializer
{
    internal class XmlSerialization
    {
        private static readonly object Locker = new object();

        /// <summary>
        /// Deserializes collection of .xml
        /// </summary>
        /// <typeparam name="T">Collection class</typeparam>
        /// <returns>Collection class</returns>
        public static T Deserialization<T>()
            where T : IList, new()
        {
            var result = new T();
            var path = string.Format(RS.Path, result.GetType().GetGenericArguments()[0].Name);
            CheckDirectoryAndCreate();
            if (!File.Exists(path)) return result;
            var serXml = new XmlSerializer(typeof(T));
            while (true)
            {
                try
                {
                    using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        result = (T)(serXml.Deserialize(fs));
                    }
                    return result;
                }
                catch (IOException)
                {
                    // LOGG
                    //Console.WriteLine(RS.IOExeption);
                    Thread.Sleep(int.Parse(RS.TimeSleep));
                }
            }
        }

        /// <summary>
        /// Serialization with append, in a separate thread
        /// </summary>
        /// <typeparam name="T">Collection class</typeparam>
        /// <param name="obj">Data</param>
        public static void Serialization<T>(T obj)
            where T : IList, new()
        {
            var thread = new Thread(() => SerializerThread(obj));
            thread.Start();
        }

        private static void SerializerThread<T>(T obj)
            where T : IList, new()
        {
            lock (Locker)
            {
                CheckDirectoryAndCreate();
                var path = string.Format(RS.Path, obj.GetType().GetGenericArguments()[0].Name);
                var serXml = new XmlSerializer(typeof(T));
                while (true)
                {
                    try
                    {
                        using (var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
                        {
                            serXml.Serialize(fs, obj);
                            return;
                        }
                    }
                    catch (IOException)
                    {
                        //Console.WriteLine(RS.IOExeption);
                    }
                }
            }
        }

        /// <summary>
        /// Checking the directory if no then create
        /// </summary>
        private static void CheckDirectoryAndCreate()
        {
            if (!Directory.Exists(RS.Directory)) Directory.CreateDirectory(RS.Directory);
        }
    }
}