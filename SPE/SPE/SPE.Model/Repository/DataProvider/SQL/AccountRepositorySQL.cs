﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SPE.Model.Repository.Common;
using SPE.Model.DataModel;
using SPE.Model.Helpers;
using SPE.Model.Services;
using System.Text;
using SPE.Model.Services.Resources;
using SPE.Model.Resources;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class AccountRepositorySQL : BaseRepository, IExpandedRepository<AccountDataModel>
    {
        public AccountRepositorySQL() { }

        #region Mapper
        private static Account GetEntity(AccountDataModel datamodel)
        {
            return Mapper.Map<Account>(datamodel);
        }

        private static AccountDataModel GetDataModel(Account entity)
        {
            return Mapper.Map<AccountDataModel>(entity);
        }
        #endregion

        #region Members IRepositoryPartical

        #region Insert

        public bool Add(AccountDataModel request)
        {
            return Insert(new List<AccountDataModel>() { request });
        }

        public bool Insert(ICollection<AccountDataModel> requests)
        {
            using (var context = GetContext())
            {
                if (Insert(context, requests))
                    if (context.SaveChanges() > 0)
                    {
                        foreach (var item in requests)
                        {
                            Logger.Info(string.Format(RS_Logger.AddedANewUserX0, item.ToString()));
                        }
                        return true;
                    }
                return false;
            }
        }

        public System.Threading.Tasks.Task<bool> InsertAsync(AccountDataModel request)
        {
            return Task.Factory.StartNew<bool>(() => Add(request));
        }

        public System.Threading.Tasks.Task<bool> InsertAsync(ICollection<AccountDataModel> requests)
        {
            return Task.Factory.StartNew<bool>(() => Insert(requests));
        }

        private bool Insert(SPEEntities context, ICollection<AccountDataModel> requests)
        {
            if (requests.Any(a => context.Accounts.Any(b => b.Login == a.Login)))
                throw new ArgumentException(RS_Message.UserExist);

            if (requests == null) return false;

            foreach (var item in requests)
            {
                if (item != null)
                {
                    var temp = GetEntity(item);
                    UpdateContext(context, temp);
                    context.Accounts.Add(temp);
                }
            }
            return true;
        }

        private bool Insert(SPEEntities context, AccountDataModel request)
        {
            return Insert(context, new List<AccountDataModel>() { request });
        }

        #endregion Insetr

        #region Remove

        public bool Remove(AccountDataModel request)
        {
            return Remove(new List<AccountDataModel>() { request });
        }

        public bool Remove(ICollection<AccountDataModel> requests)
        {
            using (var context = GetContext())
            {
                if (Remove(context, requests))
                    if (context.SaveChanges() > 0)
                    {
                        foreach (var item in requests)
                        {
                            Logger.Info(string.Format(RS_Logger.DeletedUserX0, item.ToString()));
                        }
                        return true;
                    }
                return false;
            }
        }

        public Task<bool> RemoveAsync(AccountDataModel request)
        {
            return Task.Factory.StartNew<bool>(() => Remove(request));
        }

        public Task<bool> RemoveAsync(ICollection<AccountDataModel> requests)
        {
            return Task.Factory.StartNew<bool>(() => Remove(requests));
        }

        private bool Remove(SPEEntities context, ICollection<AccountDataModel> requests)
        {
            var entities = requests.Select(a => Get(context).FirstOrDefault(b => b.Login == a.Login)).ToList();
            if (entities.Count > 0)
            {
                foreach (var item in entities)
                {
                    context.Accounts.Remove(item);
                }
                return true;
            }
            return false;
        }

        private bool Remove(SPEEntities context, AccountDataModel request)
        {
            return Remove(context, new List<AccountDataModel>() { request });
        }

        #endregion Remove

        #region Update

        public bool Update(AccountDataModel request)
        {
            return Update(new List<AccountDataModel>() { request });
        }

        public bool Update(ICollection<AccountDataModel> requests)
        {
            using (var context = GetContext())
            {
                var datamodel = requests.ToList();
                var entities = requests.Select(a => Get(context).FirstOrDefault(b => b.Login == a.Login)).ToList();
                var logMessage = new StringBuilder();


                for (int i = 0; i < requests.Count; i++)
                {
                    if (entities[i] == null) continue;

                    if (!datamodel[i].Equals(GetDataModel(entities[i])))
                    {
                        logMessage.AppendLine(string.Format(RS_Logger.UpdatAccount, entities[i].Login, entities[i].IsEnable, GetDataModel(entities[i]).ToString(), entities[i].Birthday,
                                                           string.Join(RS_Message.Separator, entities[i].Roles.Select(a => a.Name)),
                                                           datamodel[i].Login, datamodel[i].IsEnable, datamodel[i].ToString(), datamodel[i].Birthday,
                                                           string.Join(RS_Message.Separator, datamodel[i].Role.Select(s => s.Name))));
                    }

                    Convert(datamodel[i], entities[i]);
                    RoleRepositorySQL.UpdateEntity(context, entities[i].Roles, datamodel[i].Role);
                    SubjectRepositorySQL.UpdateEntity(context, entities[i].Subjects, datamodel[i].Subject);
                    LinkAccountTestRepositorySQL.UpdateEntity(context, entities[i].LinkAccountTests, datamodel[i].LinkAccountTest);
                    LinkAccountTestRepositorySQL.UpdateEntity(context, entities[i].LinkAccountTests1, datamodel[i].LinkAccountTest_1);
                    TestResultRepositorySQL.UpdateEntity(context, entities[i].TestResults, datamodel[i].TestResults);
                }

                try
                {
                    var qq = context.SaveChanges();
                    if (logMessage.Length > 0)
                    {
                        Logger.Info(logMessage.ToString());
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return false;
                }
            }
        }

        public System.Threading.Tasks.Task<bool> UpdateAsync(AccountDataModel request)
        {
            return Task.Factory.StartNew<bool>(() => Update(request));
        }

        public System.Threading.Tasks.Task<bool> UpdateAsync(ICollection<AccountDataModel> requests)
        {
            return Task.Factory.StartNew<bool>(() => Update(requests));
        }

        #endregion Update

        #region Get
        public ICollection<AccountDataModel> GetEntities()
        {
            using (var context = GetContext())
            {
                return GetEntities(context);
            }
        }

        public ICollection<AccountDataModel> GetEntities(SPEEntities context)
        {
            var entities = Get(context).ToList();
            if (entities != null)
            {
                return entities.ConvertAll(GetDataModel);
            }
            throw new ArgumentException(string.Format(RS_Message.UnknownX0, RS_Message.Account));
        }

        public AccountDataModel Get(object primaryKey)
        {
            AccountDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = Mapper.Map<AccountDataModel>(context.Roles.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }


        private static DbQuery<Account> Get(SPEEntities context)
        {
            return context.Accounts.Include(RS_Entity.LinkAccountTests)
                                                           .Include(RS_Entity.LinkAccountTests1)
                                                           .Include(RS_Entity.TestResults)
                                                           .Include(RS_Entity.Roles)
                                                           .Include(RS_Entity.Subjects);
        }

        public System.Threading.Tasks.Task<ICollection<AccountDataModel>> GetAsync()
        {
            return Task.Factory.StartNew<ICollection<AccountDataModel>>(() => GetEntities());
        }

        public object Get(Func<ICollection<AccountDataModel>, object> evaluator)
        {
            return evaluator(GetEntities());
        }

        public System.Threading.Tasks.Task<object> GetAsync(Func<ICollection<AccountDataModel>, object> evaluator)
        {
            return Task.Factory.StartNew<object>(() => Get(evaluator));
        }
        #endregion Get

        #endregion Members IRepositoryPartical

        #region Methods

        private static void UpdateContext(SPEEntities context, Account entity)
        {
            RoleRepositorySQL.UpdateEntity(context, entity.Roles);
            SubjectRepositorySQL.UpdateEntity(context, entity.Subjects);
            LinkAccountTestRepositorySQL.UpdateEntity(context, entity.LinkAccountTests);
            LinkAccountTestRepositorySQL.UpdateEntity(context, entity.LinkAccountTests1);
            TestResultRepositorySQL.UpdateEntity(context, entity.TestResults);
        }

        private static void Convert(AccountDataModel datamodel, Account entity)
        {
            entity.Login = datamodel.Login;
            entity.Password = datamodel.Password;
            entity.Name = datamodel.Name;
            entity.MiddleName = datamodel.MiddleName;
            entity.LastName = datamodel.LastName;
            entity.Birthday = datamodel.Birthday;
            entity.IsActive = datamodel.IsActive;
            entity.IsEnable = datamodel.IsEnable;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<Account> edmAccounts, ICollection<AccountDataModel> dmAccounts)
        {
            var edmNew = new List<Account>();

            foreach (var item in dmAccounts)
            {
                edmNew.Add(context.Accounts.FirstOrDefault(a => a.AccountId == item.AccountId));
            }
            var DelRole = edmAccounts.Except(edmNew);
            var AddRole = edmNew.Except(edmAccounts);

            DelRole.ToList().ForEach(a => edmAccounts.Remove(a));
            foreach (var item in AddRole)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.Accounts.Attach(item);
                edmAccounts.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<Account> edmAccounts)
        {
            var temp = new List<Account>();
            foreach (var item in edmAccounts)
            {
                temp.Add(context.Accounts.FirstOrDefault(a => a.AccountId == item.AccountId));
            }
            edmAccounts.Clear();
            temp.ForEach(a => edmAccounts.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, Account edmAccount)
        {
            if (edmAccount != null)
            {
                edmAccount = context.Accounts.FirstOrDefault(a => a.AccountId == edmAccount.AccountId);
            }
        }

        public static void UpdateEntity(SPEEntities context, Account account, int accountId)
        {
            account = context.Accounts.FirstOrDefault(a => a.AccountId == accountId);
        }

        #endregion UpdateEntity

        #endregion Methods



    }
}