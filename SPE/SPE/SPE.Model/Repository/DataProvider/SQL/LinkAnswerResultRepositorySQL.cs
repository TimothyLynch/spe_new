﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class LinkAnswerResultRepositorySQL : BaseRepository, IRepository<LinkAnswerResultDataModel>
    {
        #region Mapper

        private LinkAnswerResult GetEntity(LinkAnswerResultDataModel datamodel)
        {
            return Mapper.Map<LinkAnswerResult>(datamodel);
        }

        private LinkAnswerResultDataModel GetDataModel(LinkAnswerResult entity)
        {
            return Mapper.Map<LinkAnswerResultDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, LinkAnswerResultDataModel datamodel)
        {
            var result = false;
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                TestResultRepositorySQL.UpdateEntity(context, entity.TestResult);
                AnswerRepositorySQL.UpdateEntity(context, entity.Answer);
                context.LinkAnswerResults.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(LinkAnswerResultDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(LinkAnswerResultDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, LinkAnswerResultDataModel datamodel)
        {
            var result = false;
            var entity = context.LinkAnswerResults.FirstOrDefault(a => a.QuestionId == datamodel.QuestionId &&
                                                                       a.TestResultId == datamodel.TestResultId &&
                                                                       a.AnswerId == datamodel.AnswerId);
            if (entity != null)
            {
                context.LinkAnswerResults.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(LinkAnswerResultDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, LinkAnswerResultDataModel datamodel)
        {
            var result = false;
            var entity = context.LinkAnswerResults.FirstOrDefault(a => a.AnswerId == datamodel.AnswerId);
            if (entity != null)
            {
                TestResultRepositorySQL.UpdateEntity(context, entity.TestResult, datamodel.TestResultId);
                AnswerRepositorySQL.UpdateEntity(context, entity.Answer, datamodel.AnswerId);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<LinkAnswerResult> edmLinkAnswerResults,
            ICollection<LinkAnswerResultDataModel> dmLinkAnswerResults)
        {
            var edmNew =
                dmLinkAnswerResults.Select(
                    item => context.LinkAnswerResults.FirstOrDefault(a => a.AnswerId == item.AnswerId)).ToList();

            var delLar = edmLinkAnswerResults.Except(edmNew);
            var addLar = edmNew.Except(edmLinkAnswerResults);

            delLar.ToList().ForEach(a => edmLinkAnswerResults.Remove(a));
            foreach (var item in addLar)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.LinkAnswerResults.Attach(item);
                edmLinkAnswerResults.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<LinkAnswerResult> edmLinkAnswerResults)
        {
            var temp =
                edmLinkAnswerResults.Select(
                    item =>
                        context.LinkAnswerResults.FirstOrDefault(
                            a =>
                                a.AnswerId == item.AnswerId && a.QuestionId == item.QuestionId &&
                                a.TestResultId == item.TestResultId)).ToList();
            edmLinkAnswerResults.Clear();
            temp.ForEach(a => edmLinkAnswerResults.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, LinkAnswerResult edmLinkAnswerResult)
        {
            if (edmLinkAnswerResult == null) return;
            edmLinkAnswerResult =
                context.LinkAnswerResults.FirstOrDefault(
                    a =>
                        a.AnswerId == edmLinkAnswerResult.AnswerId && a.QuestionId == edmLinkAnswerResult.QuestionId &&
                        a.TestResultId == edmLinkAnswerResult.TestResultId);
        }

        #endregion

        public LinkAnswerResultDataModel Get(object primaryKey)
        {
            LinkAnswerResultDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.LinkAnswerResults.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<LinkAnswerResultDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.LinkAnswerResults.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<LinkAnswerResultDataModel> GetEntities()
        {
            ICollection<LinkAnswerResultDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion
    }
}