﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class ThemeRepositorySQL : BaseRepository, IRepository<ThemeDataModel>
    {
        #region Mapper

        private Theme GetEntity(ThemeDataModel datamodel)
        {
            return Mapper.Map<Theme>(datamodel);
        }

        private ThemeDataModel GetDataModel(Theme entity)
        {
            return Mapper.Map<ThemeDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, ThemeDataModel datamodel)
        {
            var result = false;
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                QuestionRepositorySQL.UpdateEntity(context, entity.Questions);
                SubjectRepositorySQL.UpdateEntity(context, entity.Subjects);
                context.Themes.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(ThemeDataModel datamodel)
        {
            var resut = false;
            using (var context = GetContext())
            {
                resut = Add(context, datamodel);
            }
            return resut;
        }

        public bool Remove(ThemeDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, ThemeDataModel datamodel)
        {
            var result = false;
            var entity = context.Themes.FirstOrDefault(a => a.ThemeId == datamodel.ThemeId);
            if (entity != null)
            {
                context.Themes.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(ThemeDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, ThemeDataModel datamodel)
        {
            var result = false;
            var entity = context.Themes.FirstOrDefault(a => a.ThemeId == datamodel.ThemeId);
            if (entity != null)
            {
                Convert(datamodel, entity);
                QuestionRepositorySQL.UpdateEntity(context, entity.Questions, datamodel.Question);
                SubjectRepositorySQL.UpdateEntity(context, entity.Subjects, datamodel.Subject);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        private static void Convert(ThemeDataModel datamodel, Theme entity)
        {
            entity.ThemeName = datamodel.ThemeName;
            entity.ThemeDesc = datamodel.ThemeDesc;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<Theme> edmThemes,
            ICollection<ThemeDataModel> dmThemes)
        {
            var edmNew = dmThemes.Select(item => context.Themes.FirstOrDefault(a => a.ThemeId == item.ThemeId)).ToList();

            var delTheme = edmThemes.Except(edmNew);
            var addTheme = edmNew.Except(edmThemes);

            delTheme.ToList().ForEach(a => edmThemes.Remove(a));
            foreach (var item in addTheme)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.Themes.Attach(item);
                edmThemes.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<Theme> edmThemes)
        {
            var temp = edmThemes.Select(item => context.Themes.FirstOrDefault(a => a.ThemeId == item.ThemeId)).ToList();
            edmThemes.Clear();
            temp.ForEach(a => edmThemes.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, Theme edmTheme)
        {
            if (edmTheme == null) return;
            edmTheme = context.Themes.FirstOrDefault(a => a.ThemeId == edmTheme.ThemeId);
        }

        #endregion

        public ThemeDataModel Get(object primaryKey)
        {
            ThemeDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.Themes.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<ThemeDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.Themes.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<ThemeDataModel> GetEntities()
        {
            ICollection<ThemeDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion
    }
}