﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class AnswerRepositorySQL : BaseRepository, IRepository<AnswerDataModel>
    {
        #region Mapper
        private Answer GetEntity(AnswerDataModel datamodel)
        {
            return Mapper.Map<Answer>(datamodel);
        }

        private AnswerDataModel GetDataModel(Answer entity)
        {
            return Mapper.Map<AnswerDataModel>(entity);
        }
        #endregion

        #region CRUD

        public bool Add(SPEEntities context, AnswerDataModel datamodel)
        {
            var result = false;
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                LinkAnswerResultRepositorySQL.UpdateEntity(context, entity.LinkAnswerResults);
                QuestionRepositorySQL.UpdateEntity(context, entity.Question);
                context.Answers.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(AnswerDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(AnswerDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, AnswerDataModel datamodel)
        {
            var result = false;
            var entity = context.Answers.FirstOrDefault(a => a.AnswerId == datamodel.AnswerId);
            if (entity != null)
            {
                context.Answers.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(SPEEntities context, AnswerDataModel datamodel)
        {
            var result = false;

            var entity = context.Answers.FirstOrDefault(a => a.AnswerId == datamodel.AnswerId);
            if (entity != null)
            {
                Convert(datamodel, entity);
                LinkAnswerResultRepositorySQL.UpdateEntity(context, entity.LinkAnswerResults, datamodel.LinkAnswerResult);
                QuestionRepositorySQL.UpdateEntity(context, entity.Question, datamodel.QuestionId);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(AnswerDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        private static void Convert(AnswerDataModel datamodel, Answer entity)
        {
            entity.AnswerText = datamodel.AnswerText;
            entity.AnswerDesc = datamodel.AnswerDesc;
        }

        #region UpdateEntity
        public static void UpdateEntity(SPEEntities context, ICollection<Answer> edmAnswers, ICollection<AnswerDataModel> dmAnswers)
        {
            var edmNew = dmAnswers.Select(item => context.Answers.FirstOrDefault(a => a.AnswerId == item.AnswerId)).ToList();

            var delAnsw = edmAnswers.Except(edmNew);
            var addAnsw = edmNew.Except(edmAnswers);

            delAnsw.ToList().ForEach(a => edmAnswers.Remove(a));
            foreach (var item in addAnsw)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.Answers.Attach(item);
                edmAnswers.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<Answer> edmAnswers)
        {
            var temp = edmAnswers.Select(item => context.Answers.FirstOrDefault(a => a.AnswerId == item.AnswerId)).ToList();
            edmAnswers.Clear();
            temp.ForEach(a => edmAnswers.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, Answer edmAnswer)
        {
            if (edmAnswer == null) return;
            context.Answers.FirstOrDefault(a => a.AnswerId == edmAnswer.AnswerId);
        }

        public static void UpdateEntity(SPEEntities context, Answer answer, int p)
        {
            context.Answers.FirstOrDefault(a => a.AnswerId == p);
        }
        #endregion

        public AnswerDataModel Get(object primaryKey)
        {
            AnswerDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.Answers.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<AnswerDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.Answers.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<AnswerDataModel> GetEntities()
        {
            ICollection<AnswerDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }
        #endregion 
    }
}