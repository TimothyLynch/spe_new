﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class QuestionRepositorySQL : BaseRepository, IRepository<QuestionDataModel>
    {
        #region Mapper

        private Question GetEntity(QuestionDataModel datamodel)
        {
            return Mapper.Map<Question>(datamodel);
        }

        private QuestionDataModel GetDataModel(Question entity)
        {
            return Mapper.Map<QuestionDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, QuestionDataModel datamodel)
        {
            var result = false;

            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                AnswerRepositorySQL.UpdateEntity(context, entity.Answers);
                LinkAnswerResultRepositorySQL.UpdateEntity(context, entity.LinkAnswerResults);
                TestRepositorySQL.UpdateEntity(context, entity.Tests);
                ThemeRepositorySQL.UpdateEntity(context, entity.Themes);
                context.Questions.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(QuestionDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(QuestionDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, QuestionDataModel datamodel)
        {
            var result = false;
            var entity = context.Questions.FirstOrDefault(a => a.IdQuestion == datamodel.IdQuestion);
            if (entity != null)
            {
                context.Questions.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(QuestionDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, QuestionDataModel datamodel)
        {
            var result = false;
            var entity = context.Questions.FirstOrDefault(a => a.IdQuestion == datamodel.IdQuestion);
            if (entity != null)
            {
                Convert(datamodel, entity);
                LinkAnswerResultRepositorySQL.UpdateEntity(context, entity.LinkAnswerResults, datamodel.LinkAnswerResult);
                TestRepositorySQL.UpdateEntity(context, entity.Tests, datamodel.Test);
                ThemeRepositorySQL.UpdateEntity(context, entity.Themes, datamodel.Theme);
                AnswerRepositorySQL.UpdateEntity(context, entity.Answers, datamodel.Answer);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        private static void Convert(QuestionDataModel datamodel, Question entity)
        {
            entity.DescQuestion = datamodel.TextQuestion;
            entity.TextQuestion = datamodel.TextQuestion;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<Question> edmQuestions,
            ICollection<QuestionDataModel> dmQuestions)
        {
            var edmNew =
                dmQuestions.Select(item => context.Questions.FirstOrDefault(a => a.IdQuestion == item.IdQuestion))
                    .ToList();

            var delQuest = edmQuestions.Except(edmNew);
            var addQuest = edmNew.Except(edmQuestions);

            delQuest.ToList().ForEach(a => edmQuestions.Remove(a));
            foreach (var item in addQuest)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.Questions.Attach(item);
                edmQuestions.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<Question> edmQuestions)
        {
            var temp =
                edmQuestions.Select(item => context.Questions.FirstOrDefault(a => a.IdQuestion == item.IdQuestion))
                    .ToList();
            edmQuestions.Clear();
            temp.ForEach(a => edmQuestions.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, Question edmQuestion)
        {
            if (edmQuestion == null) return;
            edmQuestion = context.Questions.FirstOrDefault(a => a.IdQuestion == edmQuestion.IdQuestion);
        }

        public static void UpdateEntity(SPEEntities context, Question question, int idQuestion)
        {
            question = context.Questions.FirstOrDefault(a => a.IdQuestion == idQuestion);
        }

        #endregion

        public QuestionDataModel Get(object primaryKey)
        {
            QuestionDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.Questions.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<QuestionDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.Questions.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<QuestionDataModel> GetEntities()
        {
            ICollection<QuestionDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion CRUD
    }
}