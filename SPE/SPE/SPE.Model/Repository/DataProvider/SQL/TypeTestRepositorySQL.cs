﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class TypeTestRepositorySQL : BaseRepository, IRepository<TypeTestDataModel>
    {
        #region Mapper

        private TypeTest GetEntity(TypeTestDataModel datamodel)
        {
            return Mapper.Map<TypeTest>(datamodel);
        }

        private TypeTestDataModel GetDataModel(TypeTest entity)
        {
            return Mapper.Map<TypeTestDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, TypeTestDataModel datamodel)
        {
            var result = false;
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                TestRepositorySQL.UpdateEntity(context, entity.Tests);
                context.TypeTests.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(TypeTestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(TypeTestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, TypeTestDataModel datamodel)
        {
            var result = false;
            var entity = context.TypeTests.FirstOrDefault(a => a.TypeTestId == datamodel.TypeTestId);
            if (entity != null)
            {
                context.TypeTests.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(TypeTestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, TypeTestDataModel datamodel)
        {
            var result = false;
            var entity = context.TypeTests.FirstOrDefault(a => a.TypeTestId == datamodel.TypeTestId);
            if (entity != null)
            {
                Convert(datamodel, entity);
                TestRepositorySQL.UpdateEntity(context, entity.Tests, datamodel.Test);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        private static void Convert(TypeTestDataModel datamodel, TypeTest entity)
        {
            entity.TypeTestName = datamodel.TypeTestName;
            entity.TypeTestDesc = datamodel.TypeTestDesc;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<TypeTest> edmTypeTests,
            ICollection<TypeTestDataModel> dmTypeTests)
        {
            var edmNew =
                dmTypeTests.Select(item => context.TypeTests.FirstOrDefault(a => a.TypeTestId == item.TypeTestId))
                    .ToList();

            var delTypeTest = edmTypeTests.Except(edmNew);
            var addTypeTest = edmNew.Except(edmTypeTests);

            delTypeTest.ToList().ForEach(a => edmTypeTests.Remove(a));
            foreach (var item in addTypeTest)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.TypeTests.Attach(item);
                edmTypeTests.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<TypeTest> edmTypeTests)
        {
            var temp =
                edmTypeTests.Select(item => context.TypeTests.FirstOrDefault(a => a.TypeTestId == item.TypeTestId))
                    .ToList();
            edmTypeTests.Clear();
            temp.ForEach(a => edmTypeTests.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, TypeTest edmTypeTest)
        {
            if (edmTypeTest == null) return;
            edmTypeTest = context.TypeTests.FirstOrDefault(a => a.TypeTestId == edmTypeTest.TypeTestId);
        }

        #endregion

        public TypeTestDataModel Get(object primaryKey)
        {
            TypeTestDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.TypeTests.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<TypeTestDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.TypeTests.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<TypeTestDataModel> GetEntities()
        {
            ICollection<TypeTestDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion
    }
}