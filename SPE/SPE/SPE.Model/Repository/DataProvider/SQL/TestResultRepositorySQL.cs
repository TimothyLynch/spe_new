﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class TestResultRepositorySQL : BaseRepository, IRepository<TestResultsDataModel>
    {
        #region Mapper

        private TestResult GetEntity(TestResultsDataModel datamodel)
        {
            return Mapper.Map<TestResult>(datamodel);
        }

        private TestResultsDataModel GetDataModel(TestResult entity)
        {
            return Mapper.Map<TestResultsDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, TestResultsDataModel datamodel)
        {
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                AccountRepositorySQL.UpdateEntity(context, entity.Account);
                LinkAnswerResultRepositorySQL.UpdateEntity(context, entity.LinkAnswerResults);
                TestRepositorySQL.UpdateEntity(context, entity.Test);
                context.TestResults.Add(entity);
                return context.SaveChanges() > 0;
            }
            return false;
        }

        public bool Add(TestResultsDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(TestResultsDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, TestResultsDataModel datamodel)
        {
            var result = false;
            var entity = context.TestResults.FirstOrDefault(a => a.TestResultId == datamodel.TestResultId);
            if (entity != null)
            {
                context.TestResults.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(TestResultsDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, TestResultsDataModel datamodel)
        {
            var result = false;
            var entity = context.TestResults.FirstOrDefault(a => a.TestResultId == datamodel.TypeTestId);
            if (entity != null)
            {
                Convert(datamodel, entity);
                AccountRepositorySQL.UpdateEntity(context, entity.Account, datamodel.AccountId);
                LinkAnswerResultRepositorySQL.UpdateEntity(context, entity.LinkAnswerResults, datamodel.LinkAnswerResult);
                TestRepositorySQL.UpdateEntity(context, entity.Test, datamodel.TestId);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        private static void Convert(TestResultsDataModel datamodel, TestResult entity)
        {
            entity.StatusTest = datamodel.StatusTest;
            entity.TimeTest = datamodel.TimeTest;
            entity.TimeToTest = datamodel.TimeToTest;
            entity.CountBadQuestion = datamodel.CountBadQuestion;
            entity.TestCountQuestion = datamodel.TestCountQuestion;
            entity.TestDateTime = datamodel.TestDateTime;
            entity.TestResult1 = datamodel.TestResult1;
            entity.TypeTestId = datamodel.TypeTestId;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<TestResult> edmTestResults,
            ICollection<TestResultsDataModel> dmTestResults)
        {
            var edmNewTestResults =
                dmTestResults.Select(
                    item => context.TestResults.FirstOrDefault(a => a.TestResultId == item.TestResultId)).ToList();

            var delTestRes = edmTestResults.Except(edmNewTestResults);
            var addTestRes = edmNewTestResults.Except(edmTestResults);

            delTestRes.ToList().ForEach(a => edmTestResults.Remove(a));
            foreach (var item in addTestRes)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.TestResults.Attach(item);
                edmTestResults.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<TestResult> edmTestResult)
        {
            var temp =
                edmTestResult.Select(
                    item => context.TestResults.FirstOrDefault(a => a.TestResultId == item.TestResultId)).ToList();
            edmTestResult.Clear();
            temp.ForEach(a => edmTestResult.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, TestResult edmTestResult)
        {
            if (edmTestResult == null) return;
            edmTestResult = context.TestResults.FirstOrDefault(a => a.TestResultId == edmTestResult.TestResultId);
        }

        public static void UpdateEntity(SPEEntities context, TestResult testResult, int p)
        {
            testResult = context.TestResults.FirstOrDefault(a => a.TestResultId == p);
        }

        #endregion

        public TestResultsDataModel Get(object primaryKey)
        {
            TestResultsDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.TestResults.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<TestResultsDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.TestResults.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<TestResultsDataModel> GetEntities()
        {
            ICollection<TestResultsDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion
    }
}