﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class LinkAccountTestRepositorySQL : BaseRepository, IRepository<LinkAccountTestDataModel>
    {
        #region Mapper

        private LinkAccountTest GetEntity(LinkAccountTestDataModel datamodel)
        {
            return Mapper.Map<LinkAccountTest>(datamodel);
        }

        private LinkAccountTestDataModel GetDataModel(LinkAccountTest entity)
        {
            return Mapper.Map<LinkAccountTestDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, LinkAccountTestDataModel datamodel)
        {
            var result = false;
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                AccountRepositorySQL.UpdateEntity(context, entity.Account);
                AccountRepositorySQL.UpdateEntity(context, entity.Account1);
                TestRepositorySQL.UpdateEntity(context, entity.Test);
                context.LinkAccountTests.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(LinkAccountTestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(LinkAccountTestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, LinkAccountTestDataModel datamodel)
        {
            var result = false;

            var entity = context.LinkAccountTests.FirstOrDefault(a => a.AssignedTestId == datamodel.AssignedTestId);
            if (entity != null)
            {
                context.LinkAccountTests.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(LinkAccountTestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, LinkAccountTestDataModel datamodel)
        {
            var result = false;
            var entity = context.LinkAccountTests.FirstOrDefault(a => a.AssignedTestId == datamodel.AssignedTestId);
            if (entity != null)
            {
                Convert(datamodel, entity);
                AccountRepositorySQL.UpdateEntity(context, entity.Account, datamodel.AccountId);
                AccountRepositorySQL.UpdateEntity(context, entity.Account1, datamodel.WhoAssignedAccountId);
                TestRepositorySQL.UpdateEntity(context, entity.Test, datamodel.TestId);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        private static void Convert(LinkAccountTestDataModel datamodel, LinkAccountTest entity)
        {
            entity.AssignedDate = datamodel.AssignedDate;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<LinkAccountTest> edmLinkAccountTests,
            ICollection<LinkAccountTestDataModel> dmLinkAccountTests)
        {
            var edmNew =
                dmLinkAccountTests.Select(
                    item => context.LinkAccountTests.FirstOrDefault(a => a.AssignedTestId == item.AssignedTestId))
                    .ToList();

            var delLat = edmLinkAccountTests.Except(edmNew);
            var addLat = edmNew.Except(edmLinkAccountTests);

            delLat.ToList().ForEach(a => edmLinkAccountTests.Remove(a));
            foreach (var item in addLat)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.LinkAccountTests.Attach(item);
                edmLinkAccountTests.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<LinkAccountTest> edmLinkAccountTests)
        {
            var temp =
                edmLinkAccountTests.Select(
                    item => context.LinkAccountTests.FirstOrDefault(a => a.AssignedTestId == item.AssignedTestId))
                    .ToList();
            edmLinkAccountTests.Clear();
            temp.ForEach(edmLinkAccountTests.Add);
        }

        public static void UpdateEntity(SPEEntities context, LinkAccountTest edmLinkAccountTest)
        {
            if (edmLinkAccountTest == null) return;
            edmLinkAccountTest =
                context.LinkAccountTests.FirstOrDefault(a => a.AssignedTestId == edmLinkAccountTest.AssignedTestId);
        }

        #endregion

        public LinkAccountTestDataModel Get(object primaryKey)
        {
            LinkAccountTestDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.LinkAccountTests.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<LinkAccountTestDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.LinkAccountTests.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<LinkAccountTestDataModel> GetEntities()
        {
            ICollection<LinkAccountTestDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion
    }
}