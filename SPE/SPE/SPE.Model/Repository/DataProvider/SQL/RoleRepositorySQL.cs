﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class RoleRepositorySQL : BaseRepository, IRepository<RoleDataModel>
    {
        #region Mapper

        private Role GetEntity(RoleDataModel datamodel)
        {
            return Mapper.Map<Role>(datamodel);
        }

        private RoleDataModel GetDataModel(Role entity)
        {
            return Mapper.Map<RoleDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(RoleDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Add(SPEEntities context, RoleDataModel datamodel)
        {
            var entity = Mapper.Map<Role>(datamodel);
            if (entity != null)
            {
                AccountRepositorySQL.UpdateEntity(context, entity.Accounts);
                context.Roles.Add(entity);
                return context.SaveChanges() > 0;
            }
            return false;
        }

        public bool Remove(RoleDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, RoleDataModel datamodel)
        {
            var result = false;
            var entity = context.Roles.FirstOrDefault(a => a.RoleId == datamodel.RoleId);
            if (entity != null)
            {
                context.Roles.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(RoleDataModel datamodel)
        {
            using (var context = GetContext())
            {
                return Update(context, datamodel);
            }
        }

        public bool Update(SPEEntities context, RoleDataModel datamodel)
        {
            var entity = context.Roles.FirstOrDefault(a => a.RoleId == datamodel.RoleId);
            if (entity == null) return false;
            Convert(datamodel, entity);
            AccountRepositorySQL.UpdateEntity(context, entity.Accounts, datamodel.Account);

            return context.SaveChanges() > 0;
        }

        private static void Convert(RoleDataModel datamodel, Role entity)
        {
            entity.Name = datamodel.Name;
            entity.Description = datamodel.Description;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<Role> edmRoles,
            ICollection<RoleDataModel> dmRoles)
        {
            var edmNew = dmRoles.Select(item => context.Roles.FirstOrDefault(a => a.RoleId == item.RoleId)).ToList();

            var delRole = edmRoles.Except(edmNew);
            var addRole = edmNew.Except(edmRoles);

            delRole.ToList().ForEach(a => edmRoles.Remove(a));
            foreach (var item in addRole)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.Roles.Attach(item);
                edmRoles.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<Role> edmRoles)
        {
            var temp = edmRoles.Select(item => context.Roles.FirstOrDefault(a => a.RoleId == item.RoleId)).ToList();
            edmRoles.Clear();
            temp.ForEach(a => edmRoles.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, Role edmRole)
        {
            if (edmRole == null) return;
            edmRole = context.Roles.FirstOrDefault(a => a.RoleId == edmRole.RoleId);
        }

        #endregion UpdateEntity

        public RoleDataModel Get(object primaryKey)
        {
            RoleDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = Mapper.Map<RoleDataModel>(context.Roles.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<RoleDataModel> GetEntities()
        {
            ICollection<RoleDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        public ICollection<RoleDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.Roles.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        #endregion
    }
}