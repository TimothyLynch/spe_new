﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class SubjectRepositorySQL : BaseRepository, IRepository<SubjectDataModel>
    {
        #region Mapper

        private Subject GetEntity(SubjectDataModel datamodel)
        {
            return Mapper.Map<Subject>(datamodel);
        }

        private SubjectDataModel GetDataModel(Subject entity)
        {
            return Mapper.Map<SubjectDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, SubjectDataModel datamodel)
        {
            var result = false;
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                AccountRepositorySQL.UpdateEntity(context, entity.Accounts);
                TestRepositorySQL.UpdateEntity(context, entity.Tests);
                ThemeRepositorySQL.UpdateEntity(context, entity.Themes);
                context.Subjects.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(SubjectDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(SubjectDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, SubjectDataModel datamodel)
        {
            var result = false;
            var entity = context.Subjects.FirstOrDefault(a => a.SubjectId == datamodel.SubjectId);
            if (entity != null)
            {
                context.Subjects.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(SubjectDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, SubjectDataModel datamodel)
        {
            var result = false;
            var entity = context.Subjects.FirstOrDefault(a => a.SubjectId == datamodel.SubjectId);
            if (entity != null)
            {
                Convert(datamodel, entity);
                AccountRepositorySQL.UpdateEntity(context, entity.Accounts, datamodel.Account);
                TestRepositorySQL.UpdateEntity(context, entity.Tests, datamodel.Test);
                ThemeRepositorySQL.UpdateEntity(context, entity.Themes, datamodel.Theme);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        private static void Convert(SubjectDataModel datamodel, Subject entity)
        {
            entity.SubjectName = datamodel.SubjectName;
            entity.SubjectDesc = datamodel.SubjectDesc;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<Subject> edmSubjects,
            ICollection<SubjectDataModel> dmSubjects)
        {
            var edmNew =
                dmSubjects.Select(item => context.Subjects.FirstOrDefault(a => a.SubjectId == item.SubjectId)).ToList();

            var delSubj = edmSubjects.Except(edmNew);
            var addSubj = edmNew.Except(edmSubjects);

            delSubj.ToList().ForEach(a => edmSubjects.Remove(a));
            foreach (var item in addSubj)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.Subjects.Attach(item);
                edmSubjects.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<Subject> edmSubjects)
        {
            var temp =
                edmSubjects.Select(item => context.Subjects.FirstOrDefault(a => a.SubjectId == item.SubjectId)).ToList();
            edmSubjects.Clear();
            temp.ForEach(a => edmSubjects.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, Subject edmSubject)
        {
            if (edmSubject == null) return;
            edmSubject = context.Subjects.FirstOrDefault(a => a.SubjectId == edmSubject.SubjectId);
        }

        public static void UpdateEntity(SPEEntities context, Subject subject, int p)
        {
            subject = context.Subjects.FirstOrDefault(a => a.SubjectId == p);
        }

        #endregion

        public SubjectDataModel Get(object primaryKey)
        {
            SubjectDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.Subjects.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<SubjectDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.Subjects.ToList();
            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<SubjectDataModel> GetEntities()
        {
            ICollection<SubjectDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion
    }
}