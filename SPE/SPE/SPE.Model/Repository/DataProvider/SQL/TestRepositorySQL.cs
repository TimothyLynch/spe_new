﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using SPE.Model.DataModel;
using SPE.Model.Repository.Common;

namespace SPE.Model.Repository.DataProvider.SQL
{
    public class TestRepositorySQL : BaseRepository, IRepository<TestDataModel>
    {
        #region Mapper

        private Test GetEntity(TestDataModel datamodel)
        {
            return Mapper.Map<Test>(datamodel);
        }

        private TestDataModel GetDataModel(Test entity)
        {
            return Mapper.Map<TestDataModel>(entity);
        }

        #endregion

        #region CRUD

        public bool Add(SPEEntities context, TestDataModel datamodel)
        {
            var result = false;
            var entity = GetEntity(datamodel);
            if (entity != null)
            {
                TestResultRepositorySQL.UpdateEntity(context, entity.TestResult);
                QuestionRepositorySQL.UpdateEntity(context, entity.Questions);
                LinkAccountTestRepositorySQL.UpdateEntity(context, entity.LinkAccountTests);
                TypeTestRepositorySQL.UpdateEntity(context, entity.TypeTests);
                SubjectRepositorySQL.UpdateEntity(context, entity.Subject);
                context.Tests.Add(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Add(TestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Add(context, datamodel);
            }
            return result;
        }

        public bool Remove(TestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Remove(context, datamodel);
            }
            return result;
        }

        public bool Remove(SPEEntities context, TestDataModel datamodel)
        {
            var result = false;
            var entity = context.Tests.FirstOrDefault(a => a.TestId == datamodel.TestId);
            if (entity != null)
            {
                context.Tests.Remove(entity);
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        public bool Update(TestDataModel datamodel)
        {
            var result = false;
            using (var context = GetContext())
            {
                result = Update(context, datamodel);
            }
            return result;
        }

        public bool Update(SPEEntities context, TestDataModel datamodel)
        {
            var result = false;
            var entity = context.Tests.FirstOrDefault(a => a.TestId == datamodel.TestId);
            if (entity != null)
            {
                Convert(datamodel, entity);
                QuestionRepositorySQL.UpdateEntity(context, entity.Questions, datamodel.Question);
                LinkAccountTestRepositorySQL.UpdateEntity(context, entity.LinkAccountTests, datamodel.LinkAccountTest);
                TypeTestRepositorySQL.UpdateEntity(context, entity.TypeTests, datamodel.TypeTest);
                SubjectRepositorySQL.UpdateEntity(context, entity.Subject, datamodel.SubjectId);
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges() > 0;
            }
            return result;
        }

        private static void Convert(TestDataModel datamodel, Test entity)
        {
            entity.TestName = datamodel.TestName;
            entity.TestDesc = datamodel.TestDesc;
            entity.TestIsEndEdit = datamodel.TestIsEndEdit;
            entity.TestCountViewQuestion = datamodel.TestCountViewQuestion;
            entity.TestSetTime = datamodel.TestSetTime;
        }

        #region UpdateEntity

        public static void UpdateEntity(SPEEntities context, ICollection<Test> edmTests,
            ICollection<TestDataModel> dmTests)
        {
            var edmNew = dmTests.Select(item => context.Tests.FirstOrDefault(a => a.TestId == item.TestId)).ToList();

            var delTest = edmTests.Except(edmNew);
            var addTest = edmNew.Except(edmTests);

            delTest.ToList().ForEach(a => edmTests.Remove(a));
            foreach (var item in addTest)
            {
                if (context.Entry(item).State == EntityState.Detached)
                    context.Tests.Attach(item);
                edmTests.Add(item);
            }
        }

        public static void UpdateEntity(SPEEntities context, ICollection<Test> edmTests)
        {
            var temp = edmTests.Select(item => context.Tests.FirstOrDefault(a => a.TestId == item.TestId)).ToList();
            edmTests.Clear();
            temp.ForEach(a => edmTests.Add(a));
        }

        public static void UpdateEntity(SPEEntities context, Test edmTest)
        {
            if (edmTest == null) return;
            edmTest = context.Tests.FirstOrDefault(a => a.TestId == edmTest.TestId);
        }

        public static void UpdateEntity(SPEEntities context, Test test, int testid)
        {
            test = context.Tests.FirstOrDefault(a => a.TestId == testid);
        }

        #endregion

        public TestDataModel Get(object primaryKey)
        {
            TestDataModel result = null;
            using (var context = GetContext())
            {
                var datamodel = GetDataModel(context.Tests.Find(primaryKey));
                if (datamodel != null)
                {
                    result = datamodel;
                }
            }
            return result;
        }

        public ICollection<TestDataModel> GetEntities(SPEEntities context)
        {
            var entities = context.Tests.ToList();

            return (from b in entities select GetDataModel(b)).ToList();
        }

        public ICollection<TestDataModel> GetEntities()
        {
            ICollection<TestDataModel> result;
            using (var context = GetContext())
            {
                result = GetEntities(context);
            }
            return result;
        }

        #endregion

        #region Helper methods

        public List<AssignedTestsModel> GetAssignedTests(int accountId, int subjectId)
        {
            using (var context = GetContext())
            {
                var tests = (from t in context.Tests
                    join a in context.LinkAccountTests on t.TestId equals a.TestId
                    join s in context.Subjects on t.SubjectId equals s.SubjectId
                    join y in context.TypeTests on t.TypeTests.FirstOrDefault(b => b.Tests.Contains(t)) equals y
                    where a.AccountId == accountId && s.SubjectId == subjectId  
                    select new AssignedTestsModel
                    {
                        TestId = t.TestId,
                        SubjectName = s.SubjectName,
                        TestName = t.TestName,
                        QuestionCount = t.TestCountViewQuestion,
                        TestIsEndEdit = t.TestIsEndEdit,
                        TestSetTime = t.TestSetTime,
                        AssignedDate = a.AssignedDate,
                        TestType = y.TypeTestName
                    }).ToList();

                return tests;
            }
        }

        public List<AssignedTestsModel> GetAllTests(int subjectId)
        {
            using (var context = GetContext())
            {
                var tests = (from t in context.Tests
                    join s in context.Subjects on t.SubjectId equals s.SubjectId
                    join y in context.TypeTests on t.TypeTests.FirstOrDefault(a => a.Tests.Contains(t)) equals y
                    where s.SubjectId == subjectId
                    select new AssignedTestsModel
                    {
                        TestId = t.TestId,
                        SubjectName = s.SubjectName,
                        TestName = t.TestName,
                        QuestionCount = t.TestCountViewQuestion,
                        TestIsEndEdit = t.TestIsEndEdit,
                        TestSetTime = t.TestSetTime,
                        TestType = y.TypeTestName
                    }).ToList();

                return tests;
            }
        }

        public List<StudentStatisticModel> GetResults(int accountId, int subjectId)
        {
            using (var context = GetContext())
            {
                var statistic = (from r in context.TestResults
                    join t in context.Tests on r.TestId equals t.TestId
                    join s in context.Subjects on t.SubjectId equals s.SubjectId
                    join y in context.TypeTests on t.TypeTests.FirstOrDefault(a => a.Tests.Contains(t)) equals y
                    where r.AccountId == accountId && s.SubjectId == subjectId
                    select new StudentStatisticModel
                    {
                        TestId = t.TestId,
                        TestName = t.TestName,
                        SubjectName = s.SubjectName,
                        PassingDate = r.TestDateTime,
                        QuestionCount = r.TestCountQuestion,
                        CountBadQuestions = r.CountBadQuestion,
                        TimeTest = r.TimeTest,
                        TimeToTest = r.TimeToTest,
                        TestType = y.TypeTestName,
                        StatusTest = (r.TestResult1) ? 1 : 0
                    }).ToList();
                return statistic;
            }
        }

        #endregion
    }
}