﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPE.Model.Repository.Common
{
    public interface IExpandedRepository<TValue> : IRepository<TValue>
        where TValue : class
    {
        #region Insert

        bool Insert(ICollection<TValue> requests);

        Task<bool> InsertAsync(TValue request);

        Task<bool> InsertAsync(ICollection<TValue> requests);

        #endregion Insert

        #region Remove

        bool Remove(ICollection<TValue> requests);

        Task<bool> RemoveAsync(TValue request);

        Task<bool> RemoveAsync(ICollection<TValue> requests);

        #endregion Remove

        #region Update

       
        bool Update(ICollection<TValue> requests);

        Task<bool> UpdateAsync(TValue request);

        Task<bool> UpdateAsync(ICollection<TValue> requests);

        #endregion Update

        #region Get

       
        Task<ICollection<TValue>> GetAsync();

        object Get(Func<ICollection<TValue>, object> evaluator);

        Task<object> GetAsync(Func<ICollection<TValue>, object> evaluator);

        #endregion Get
    }
}