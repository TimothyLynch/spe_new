﻿using System.Collections.Generic;

namespace SPE.Model.Repository.Common
{
    public interface IRepository<TData> where TData : class
    {
        #region Methods
        bool Add(TData datamodel);

        bool Remove(TData datamodel);

        bool Update(TData datamodel);

        TData Get(object primaryKey);

        ICollection<TData> GetEntities();
        #endregion
    }
}