﻿using SPE.Model.DataModel;
using System.Collections.Generic;

namespace SPE.Model.Services.Interface
{
    public interface ITeacherService
    {
        /// <summary>
        /// Get all statistics on the students
        /// </summary>
        /// <returns>List all results</returns>
        ICollection<TestResultsDataModel> GetAllStatistics();

        /// <summary>
        /// Get list of subjects teacher
        /// </summary>
        /// <returns>List all subjects of teacher</returns>
        ICollection<SubjectDataModel> GetSubjects();

        /// <summary>
        /// Get list of themes subject
        /// </summary>
        /// <param name="subjectId">this is subjectId of subject</param>
        /// <returns>List all theme of subject</returns>
        ICollection<ThemeDataModel> GetThemes(int subjectId);

        /// <summary>
        /// Get list of questions theme
        /// </summary>
        /// <param name="themeId">this is themeId of theme</param>
        /// <returns>List all question of theme</returns>
        ICollection<QuestionDataModel> GetQuestions(int themeId);

        /// <summary>
        /// Get list of answers question
        /// </summary>
        /// <param name="questionId">this is questionId of answer</param>
        /// <returns>List all answer of question</returns>
        ICollection<AnswerDataModel> GetAnswers(int questionId);
    }
}