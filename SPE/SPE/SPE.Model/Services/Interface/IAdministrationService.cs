﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SPE.Model.DataModel;

namespace SPE.Model.Services.Interface
{
    public interface IAdministrationService
    {
        AccountDataModel Authorization(string login, string password);

        Task<AccountDataModel> AuthorizationAsync(string login, string password);

        bool RegistrationAccount(AccountDataModel value);

        Task<bool> RegistrationAccountAsync(AccountDataModel value);

        void AccountIsActive(AccountDataModel account, bool state);

        Task AccountIsActiveAsync(AccountDataModel account, bool state);

        bool UpdateAllAccount(ICollection<AccountDataModel> accounts);

        Task<bool> UpdateAllAccountAsync(ICollection<AccountDataModel> accounts);
    }
}