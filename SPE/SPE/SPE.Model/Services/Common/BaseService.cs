﻿namespace SPE.Model.Services.Common
{
    public abstract class BaseService
    {
        private SPEEntities Context { get; set; }

        protected SPEEntities GetContext()
        {
            return Context ?? (Context = new SPEEntities());
        }
    }
}
