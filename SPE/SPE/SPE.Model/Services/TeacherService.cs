﻿using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Model.Services.Interface;
using SPE.Model.Services.Common;

namespace SPE.Model.Services
{
    public class TeacherService : BaseService, ITeacherService
    {
        #region Constructor
        public TeacherService(int teacherId)
        {
            TeacherId = teacherId;
        }
        #endregion Constructor

        #region Properies
        private int TeacherId { get; set; }
        #endregion

        #region ITeacherService
        #region Methods
        #region GetMethods
        public ICollection<TestResultsDataModel> GetAllStatistics()
        {
            var linkAccountTests = new LinkAccountTestRepositorySQL().GetEntities(GetContext()).Where(
                    a => a.WhoAssignedAccountId == TeacherId
                ).ToList();
            var testResults = new TestResultRepositorySQL().GetEntities(GetContext());

            return linkAccountTests.Select(item => testResults.FirstOrDefault(a => a.AccountId.Equals(item.AccountId))).Where(testResultDM => testResultDM != null).ToList();
        }

        public ICollection<SubjectDataModel> GetSubjects()
        {
            return new SubjectRepositorySQL().GetEntities(GetContext()).Where(
                        a => a.Account.Contains(a.Account.FirstOrDefault(
                            b => b.AccountId == TeacherId)
                        )).ToList();
        }

        public ICollection<ThemeDataModel> GetThemes(int subjectId)
        {
            return new ThemeRepositorySQL().GetEntities(GetContext()).Where(
                        a => a.Subject.Contains(a.Subject.FirstOrDefault(
                            b => b.SubjectId == subjectId)
                            ))
                        .ToList();
        }

        public ICollection<QuestionDataModel> GetQuestions(int themeId)
        {
            return new QuestionRepositorySQL().GetEntities(GetContext()).Where(
                a => a.Theme.Contains(a.Theme.FirstOrDefault(
                    b => b.ThemeId == themeId)
                    ))
                    .ToList();
        }

        public ICollection<AnswerDataModel> GetAnswers(int questionId)
        {
             return new AnswerRepositorySQL().GetEntities(GetContext()).Where(
                a => a.Question.IdQuestion == questionId)                    
                .ToList();
        }

        public ICollection<TypeTestDataModel> GetTypeTests()
        {
            return new TypeTestRepositorySQL().GetEntities(GetContext());
        }

        public ICollection<TestDataModel> GetTests(int accountId)
        {
            return new TestRepositorySQL().GetEntities(GetContext()).Where(
                      a => a.Subject.Account.Contains(a.Subject.Account.FirstOrDefault(
                          b => b.AccountId == accountId)
                          ))
                          .ToList();
        }

        public ICollection<LinkAccountTestDataModel> GetLinkAccountTests()
        {
            return new LinkAccountTestRepositorySQL().GetEntities(GetContext());
        }

        public ICollection<AccountDataModel> GetAccounts()
        {
            return new AccountRepositorySQL().GetEntities(GetContext());
        }
        #endregion

        #region AddMethods
        public bool AddLinkAccountTest(LinkAccountTestDataModel linkAccountTestDM)
        {
            if (linkAccountTestDM == null) return false;

            linkAccountTestDM.Account = null;
            linkAccountTestDM.Account_1 = null;
            linkAccountTestDM.Test = null;
            linkAccountTestDM.WhoAssignedAccountId = TeacherId;

            return new LinkAccountTestRepositorySQL().Add(GetContext(), linkAccountTestDM);
        }

        public bool AddTest(TestDataModel testDM)
        {
            if (testDM == null) return false;

            testDM.Question.Clear();
            testDM.LinkAccountTest.Clear();
            testDM.Subject = null;

            var themes = new ThemeRepositorySQL().GetEntities(GetContext()).Where(
                a => a.Subject.Contains(a.Subject.FirstOrDefault(
                    b => b.SubjectId == testDM.SubjectId)
                    )).ToList();

            for (int cnt = 0; cnt < themes.Count; cnt++)
            {
                if (themes.ElementAt(cnt).Question.Count <= 0) continue;
                for (int cntq = 0; cntq < themes.ElementAt(cnt).Question.Count; cntq++)
                {
                    testDM.Question.Add(themes.ElementAt(cnt).Question.ElementAt(cntq));
                }
            }

            return new TestRepositorySQL().Add(GetContext(), testDM);
        }

        public bool AddSubject(SubjectDataModel subjectDM)
        {
            if (subjectDM == null) return false;

            subjectDM.Account.Clear(); 
            subjectDM.Test.Clear(); 
            subjectDM.Theme.Clear();

            var account = new AccountRepositorySQL().GetEntities(GetContext()).FirstOrDefault(
                a => a.AccountId == TeacherId);
            subjectDM.Account.Add(account);

            return new SubjectRepositorySQL().Add(GetContext(), subjectDM);
        }

        public bool AddTheme(ThemeDataModel themeDM, int subjectId)
        {
            if (themeDM == null) return false;

            themeDM.Question.Clear();
            themeDM.Subject.Clear();

            var subject = new SubjectRepositorySQL().GetEntities(GetContext()).FirstOrDefault(
                    a => a.SubjectId == subjectId);

            themeDM.Subject.Add(subject);

            return new ThemeRepositorySQL().Add(GetContext(), themeDM);
        }

        public bool AddQuestion(QuestionDataModel questionsDM, int themeId)
        {
            if (questionsDM == null) return false;

            questionsDM.Answer.Clear();
            questionsDM.LinkAnswerResult.Clear();
            questionsDM.Test.Clear();
            questionsDM.Theme.Clear();

            var theme = new ThemeRepositorySQL().GetEntities(GetContext()).FirstOrDefault(
                    a => a.ThemeId == themeId);

            questionsDM.Theme.Add(theme);

            return new QuestionRepositorySQL().Add(GetContext(), questionsDM);
        }

        public bool AddAnswer(AnswerDataModel answersDM, int questionId, int typeAnswerValue)
        {
            if (answersDM == null) return false;

            answersDM.Question = null;
            answersDM.QuestionId = questionId;

            return new AnswerRepositorySQL().Add(GetContext(), answersDM); 
        }
        #endregion AddMethods
        #endregion Methods
        #endregion ITeacherService
    }
}

