﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SPE.Model.DataModel;
using SPE.Model.Helpers;
using SPE.Model.Repository.Common;
using SPE.Model.Services.Interface;
using SPE.Model.Resources;

namespace SPE.Model.Services
{
    public class AdministrationService : IAdministrationService
    {
        private readonly IExpandedRepository<AccountDataModel> _providerAccount;

        public AdministrationService(IExpandedRepository<AccountDataModel> providerAccount)
        {
            _providerAccount = providerAccount;
        }

        #region Members IAccountService

        public AccountDataModel Authorization(string login, string password)
        {
            var hashPass = GetHashPasswors(password);
            return
                (AccountDataModel)
                    _providerAccount.Get(
                        a => a.FirstOrDefault(b => b.Login.Equals(login) && b.Password.Equals(hashPass)));
        }

        public Task<AccountDataModel> AuthorizationAsync(string login, string password)
        {
            return Task.Factory.StartNew<AccountDataModel>(() => Authorization(login, password));
        }

        public bool RegistrationAccount(AccountDataModel value)
        {
            value.Password = GetHashPasswors(value.Password);
            value.IsEnable = true;

            return _providerAccount.Add(value);
        }

        public Task<bool> RegistrationAccountAsync(AccountDataModel value)
        {
            return Task.Factory.StartNew<bool>(() => RegistrationAccount(value));
        }

        public ICollection<AccountDataModel> GetAccountsWithoutRoles()
        {
            return _providerAccount.GetEntities().Where(a => a.Role.Count == 0).ToList();
        }

        public void AccountIsActive(AccountDataModel accoun, bool state)
        {
            accoun.IsActive = state;
            var result = _providerAccount.Update(accoun);
            if (!result) return;
            Logger.Info(state
                ? string.Format(RS_Logger.ActiveAccount, accoun.ToString())
                : string.Format(RS_Logger.InactiveAccount, accoun.ToString()));
        }

        public Task AccountIsActiveAsync(AccountDataModel account, bool state)
        {
            return Task.Factory.StartNew(() => AccountIsActive(account, state));
        }

        public void ChangePass(AccountDataModel account)
        {
            account.Password = GetHashPasswors(account.Password);
            _providerAccount.Update(account);
        }

        public bool UpdateAllAccount(ICollection<AccountDataModel> accounts)
        {
            if (accounts == null) return false;
            var allAccount = _providerAccount.GetEntities();
            var insertAccount = accounts.Where(a => allAccount.FirstOrDefault(b => b.Login == a.Login) == null).ToList();
            var removeAccount = allAccount.Where(a => accounts.FirstOrDefault(b => b.Login == a.Login) == null).ToList();

            _providerAccount.Remove(removeAccount);
            _providerAccount.Insert(insertAccount);
            _providerAccount.Update(accounts);

            return true;
        }

        public Task<bool> UpdateAllAccountAsync(ICollection<AccountDataModel> accounts)
        {
            return Task.Factory.StartNew(() => UpdateAllAccount(accounts));
        }

        #endregion Members IAccountService

        #region Helper methods

        public static string GetHashPasswors(string pass)
        {
            var result = new StringBuilder();
            var bytes = Encoding.Unicode.GetBytes(pass);
            var csp = new MD5CryptoServiceProvider();
            var byteHash = csp.ComputeHash(bytes);
            foreach (var item in byteHash)
                result.AppendFormat("{0:x2}", item);
            return result.ToString();
        }

        #endregion Helper methods
    }
}