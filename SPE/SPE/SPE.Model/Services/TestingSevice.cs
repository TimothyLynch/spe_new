﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Model.Services.Common;

namespace SPE.Model.Services
{
    public class TestingSevice : BaseService
    {
        #region GetMethods
        public ICollection<QuestionDataModel> GetQuestionFromTest(int testId)
        {
            return new TestRepositorySQL().GetEntities(GetContext()).FirstOrDefault(a => a.TestId == testId).Question;
        }

        public ICollection<QuestionDataModel> GetQuestionFromTestRandom(int testId)
        {
            var testCountViewQuestion = new TestRepositorySQL().Get(testId).TestCountViewQuestion;
            var allQuestionInTest = GetQuestionFromTest(testId).Count;
            Random rnd = new Random();
            return (testCountViewQuestion < allQuestionInTest) ? GetQuestionFromTest(testId).OrderBy(a => rnd.Next()).Take(testCountViewQuestion).ToList() : GetQuestionFromTest(testId).ToList();
        }

        public TimeSpan? GetTimeForTest(int testId)
        {
            return new TestRepositorySQL().GetEntities(GetContext()).FirstOrDefault(a => a.TestId == testId).TestSetTime;
        }

        public int GetTypeTestIdByTestId(int testId)
        {
            return new TestRepositorySQL().GetEntities(GetContext()).FirstOrDefault(a => a.TestId == testId).TypeTest.FirstOrDefault().TypeTestId;
        }

        public string GetTypeTestNameByTestId(int testId)
        {
            return new TestRepositorySQL().GetEntities(GetContext()).FirstOrDefault(a => a.TestId == testId).TypeTest.FirstOrDefault().TypeTestName;
        }

        public bool AddTestResult(TestResultsDataModel testResultDM)
        {
            return new TestResultRepositorySQL().Add(GetContext(), testResultDM);
        }

        public AccountDataModel GetAccount(int accountId)
        {
            return new AccountRepositorySQL().GetEntities(GetContext()).FirstOrDefault(a => a.AccountId == accountId);
        }
        #endregion GetMethods
    }
}
