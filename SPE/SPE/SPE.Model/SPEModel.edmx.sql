
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 06/30/2014 22:49:24
-- Generated from EDMX file: C:\Users\Sunny\Desktop\SPE\SPE\SPE.Model\SPEModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [db_SPE];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_LinkAccountTest_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAccountTests] DROP CONSTRAINT [FK_LinkAccountTest_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkAccountTest_Account1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAccountTests] DROP CONSTRAINT [FK_LinkAccountTest_Account1];
GO
IF OBJECT_ID(N'[dbo].[FK_TestResult_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TestResults] DROP CONSTRAINT [FK_TestResult_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkAnswerResult_Answer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAnswerResults] DROP CONSTRAINT [FK_LinkAnswerResult_Answer];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkAccountTest_Test]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAccountTests] DROP CONSTRAINT [FK_LinkAccountTest_Test];
GO
IF OBJECT_ID(N'[dbo].[FK_AnswerToResult_TestResult]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAnswerResults] DROP CONSTRAINT [FK_AnswerToResult_TestResult];
GO
IF OBJECT_ID(N'[dbo].[FK_Test_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Tests] DROP CONSTRAINT [FK_Test_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_TestResult_Test]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TestResults] DROP CONSTRAINT [FK_TestResult_Test];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkAccountRole_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAccountRole] DROP CONSTRAINT [FK_LinkAccountRole_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkAccountRole_Role]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAccountRole] DROP CONSTRAINT [FK_LinkAccountRole_Role];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkAccountSubject_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAccountSubject] DROP CONSTRAINT [FK_LinkAccountSubject_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkAccountSubject_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAccountSubject] DROP CONSTRAINT [FK_LinkAccountSubject_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkQuestionTest_Test]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkQuestionTest] DROP CONSTRAINT [FK_LinkQuestionTest_Test];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkQuestionTest_Question]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkQuestionTest] DROP CONSTRAINT [FK_LinkQuestionTest_Question];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkTestToTypeTest_Test]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkTestToTypeTest] DROP CONSTRAINT [FK_LinkTestToTypeTest_Test];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkTestToTypeTest_TypeTest]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkTestToTypeTest] DROP CONSTRAINT [FK_LinkTestToTypeTest_TypeTest];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkThemeQuestion_Question]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkThemeQuestion] DROP CONSTRAINT [FK_LinkThemeQuestion_Question];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkThemeQuestion_Theme]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkThemeQuestion] DROP CONSTRAINT [FK_LinkThemeQuestion_Theme];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkThemeSubject_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkThemeSubject] DROP CONSTRAINT [FK_LinkThemeSubject_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_LinkThemeSubject_Theme]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkThemeSubject] DROP CONSTRAINT [FK_LinkThemeSubject_Theme];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionLinkAnswerResult]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LinkAnswerResults] DROP CONSTRAINT [FK_QuestionLinkAnswerResult];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answers] DROP CONSTRAINT [FK_QuestionAnswer];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Accounts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Accounts];
GO
IF OBJECT_ID(N'[dbo].[Answers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Answers];
GO
IF OBJECT_ID(N'[dbo].[LinkAccountTests]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkAccountTests];
GO
IF OBJECT_ID(N'[dbo].[LinkAnswerResults]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkAnswerResults];
GO
IF OBJECT_ID(N'[dbo].[Questions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Questions];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[Subjects]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Subjects];
GO
IF OBJECT_ID(N'[dbo].[Tests]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tests];
GO
IF OBJECT_ID(N'[dbo].[TestResults]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TestResults];
GO
IF OBJECT_ID(N'[dbo].[Themes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Themes];
GO
IF OBJECT_ID(N'[dbo].[TypeTests]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TypeTests];
GO
IF OBJECT_ID(N'[dbo].[LinkAccountRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkAccountRole];
GO
IF OBJECT_ID(N'[dbo].[LinkAccountSubject]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkAccountSubject];
GO
IF OBJECT_ID(N'[dbo].[LinkQuestionTest]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkQuestionTest];
GO
IF OBJECT_ID(N'[dbo].[LinkTestToTypeTest]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkTestToTypeTest];
GO
IF OBJECT_ID(N'[dbo].[LinkThemeQuestion]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkThemeQuestion];
GO
IF OBJECT_ID(N'[dbo].[LinkThemeSubject]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LinkThemeSubject];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Accounts'
CREATE TABLE [dbo].[Accounts] (
    [AccountId] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(50)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [MiddleName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(100)  NOT NULL,
    [Birthday] datetime  NULL,
    [IsActive] bit  NOT NULL,
    [IsEnable] bit  NOT NULL
);
GO

-- Creating table 'Answers'
CREATE TABLE [dbo].[Answers] (
    [AnswerId] int IDENTITY(1,1) NOT NULL,
    [AnswerText] nvarchar(max)  NOT NULL,
    [AnswerDesc] nvarchar(max)  NULL,
    [TypeAnswerValue] int  NOT NULL,
    [QuestionId] int  NOT NULL
);
GO

-- Creating table 'LinkAccountTests'
CREATE TABLE [dbo].[LinkAccountTests] (
    [AssignedTestId] int IDENTITY(1,1) NOT NULL,
    [TestId] int  NOT NULL,
    [AccountId] int  NOT NULL,
    [AssignedDate] datetime  NOT NULL,
    [WhoAssignedAccountId] int  NOT NULL
);
GO

-- Creating table 'LinkAnswerResults'
CREATE TABLE [dbo].[LinkAnswerResults] (
    [TestResultId] int  NOT NULL,
    [QuestionId] int  NOT NULL,
    [AnswerId] int  NOT NULL
);
GO

-- Creating table 'Questions'
CREATE TABLE [dbo].[Questions] (
    [IdQuestion] int IDENTITY(1,1) NOT NULL,
    [TextQuestion] nvarchar(max)  NOT NULL,
    [DescQuestion] nvarchar(max)  NOT NULL,
    [TypeQuestion] int  NOT NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [RoleId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Moderation] bit  NOT NULL
);
GO

-- Creating table 'Subjects'
CREATE TABLE [dbo].[Subjects] (
    [SubjectId] int IDENTITY(1,1) NOT NULL,
    [SubjectName] nvarchar(max)  NOT NULL,
    [SubjectDesc] nvarchar(max)  NULL
);
GO

-- Creating table 'Tests'
CREATE TABLE [dbo].[Tests] (
    [TestId] int IDENTITY(1,1) NOT NULL,
    [TestName] nvarchar(max)  NOT NULL,
    [TestDesc] nvarchar(max)  NULL,
    [TestIsEndEdit] bit  NOT NULL,
    [TestCountViewQuestion] int  NOT NULL,
    [TestSetTime] time  NULL,
    [SubjectId] int  NOT NULL
);
GO

-- Creating table 'TestResults'
CREATE TABLE [dbo].[TestResults] (
    [TestResultId] int IDENTITY(1,1) NOT NULL,
    [TestId] int  NOT NULL,
    [AccountId] int  NOT NULL,
    [TypeTestId] int  NOT NULL,
    [TestDateTime] datetime  NOT NULL,
    [TestCountQuestion] int  NOT NULL,
    [CountBadQuestion] int  NOT NULL,
    [TimeToTest] time  NOT NULL,
    [TimeTest] time  NOT NULL,
    [StatusTest] int  NOT NULL,
    [TestResult1] bit  NOT NULL
);
GO

-- Creating table 'Themes'
CREATE TABLE [dbo].[Themes] (
    [ThemeId] int IDENTITY(1,1) NOT NULL,
    [ThemeName] nvarchar(max)  NOT NULL,
    [ThemeDesc] nvarchar(max)  NULL
);
GO

-- Creating table 'TypeTests'
CREATE TABLE [dbo].[TypeTests] (
    [TypeTestId] int IDENTITY(1,1) NOT NULL,
    [TypeTestName] nvarchar(max)  NOT NULL,
    [TypeTestDesc] nvarchar(max)  NULL
);
GO

-- Creating table 'LinkAccountRole'
CREATE TABLE [dbo].[LinkAccountRole] (
    [Accounts_AccountId] int  NOT NULL,
    [Roles_RoleId] int  NOT NULL
);
GO

-- Creating table 'LinkAccountSubject'
CREATE TABLE [dbo].[LinkAccountSubject] (
    [Accounts_AccountId] int  NOT NULL,
    [Subjects_SubjectId] int  NOT NULL
);
GO

-- Creating table 'LinkQuestionTest'
CREATE TABLE [dbo].[LinkQuestionTest] (
    [Tests_TestId] int  NOT NULL,
    [Questions_IdQuestion] int  NOT NULL
);
GO

-- Creating table 'LinkTestToTypeTest'
CREATE TABLE [dbo].[LinkTestToTypeTest] (
    [Tests_TestId] int  NOT NULL,
    [TypeTests_TypeTestId] int  NOT NULL
);
GO

-- Creating table 'LinkThemeQuestion'
CREATE TABLE [dbo].[LinkThemeQuestion] (
    [Questions_IdQuestion] int  NOT NULL,
    [Themes_ThemeId] int  NOT NULL
);
GO

-- Creating table 'LinkThemeSubject'
CREATE TABLE [dbo].[LinkThemeSubject] (
    [Subjects_SubjectId] int  NOT NULL,
    [Themes_ThemeId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AccountId] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [PK_Accounts]
    PRIMARY KEY CLUSTERED ([AccountId] ASC);
GO

-- Creating primary key on [AnswerId] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [PK_Answers]
    PRIMARY KEY CLUSTERED ([AnswerId] ASC);
GO

-- Creating primary key on [AssignedTestId] in table 'LinkAccountTests'
ALTER TABLE [dbo].[LinkAccountTests]
ADD CONSTRAINT [PK_LinkAccountTests]
    PRIMARY KEY CLUSTERED ([AssignedTestId] ASC);
GO

-- Creating primary key on [TestResultId], [QuestionId], [AnswerId] in table 'LinkAnswerResults'
ALTER TABLE [dbo].[LinkAnswerResults]
ADD CONSTRAINT [PK_LinkAnswerResults]
    PRIMARY KEY CLUSTERED ([TestResultId], [QuestionId], [AnswerId] ASC);
GO

-- Creating primary key on [IdQuestion] in table 'Questions'
ALTER TABLE [dbo].[Questions]
ADD CONSTRAINT [PK_Questions]
    PRIMARY KEY CLUSTERED ([IdQuestion] ASC);
GO

-- Creating primary key on [RoleId] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([RoleId] ASC);
GO

-- Creating primary key on [SubjectId] in table 'Subjects'
ALTER TABLE [dbo].[Subjects]
ADD CONSTRAINT [PK_Subjects]
    PRIMARY KEY CLUSTERED ([SubjectId] ASC);
GO

-- Creating primary key on [TestId] in table 'Tests'
ALTER TABLE [dbo].[Tests]
ADD CONSTRAINT [PK_Tests]
    PRIMARY KEY CLUSTERED ([TestId] ASC);
GO

-- Creating primary key on [TestResultId] in table 'TestResults'
ALTER TABLE [dbo].[TestResults]
ADD CONSTRAINT [PK_TestResults]
    PRIMARY KEY CLUSTERED ([TestResultId] ASC);
GO

-- Creating primary key on [ThemeId] in table 'Themes'
ALTER TABLE [dbo].[Themes]
ADD CONSTRAINT [PK_Themes]
    PRIMARY KEY CLUSTERED ([ThemeId] ASC);
GO

-- Creating primary key on [TypeTestId] in table 'TypeTests'
ALTER TABLE [dbo].[TypeTests]
ADD CONSTRAINT [PK_TypeTests]
    PRIMARY KEY CLUSTERED ([TypeTestId] ASC);
GO

-- Creating primary key on [Accounts_AccountId], [Roles_RoleId] in table 'LinkAccountRole'
ALTER TABLE [dbo].[LinkAccountRole]
ADD CONSTRAINT [PK_LinkAccountRole]
    PRIMARY KEY NONCLUSTERED ([Accounts_AccountId], [Roles_RoleId] ASC);
GO

-- Creating primary key on [Accounts_AccountId], [Subjects_SubjectId] in table 'LinkAccountSubject'
ALTER TABLE [dbo].[LinkAccountSubject]
ADD CONSTRAINT [PK_LinkAccountSubject]
    PRIMARY KEY NONCLUSTERED ([Accounts_AccountId], [Subjects_SubjectId] ASC);
GO

-- Creating primary key on [Tests_TestId], [Questions_IdQuestion] in table 'LinkQuestionTest'
ALTER TABLE [dbo].[LinkQuestionTest]
ADD CONSTRAINT [PK_LinkQuestionTest]
    PRIMARY KEY NONCLUSTERED ([Tests_TestId], [Questions_IdQuestion] ASC);
GO

-- Creating primary key on [Tests_TestId], [TypeTests_TypeTestId] in table 'LinkTestToTypeTest'
ALTER TABLE [dbo].[LinkTestToTypeTest]
ADD CONSTRAINT [PK_LinkTestToTypeTest]
    PRIMARY KEY NONCLUSTERED ([Tests_TestId], [TypeTests_TypeTestId] ASC);
GO

-- Creating primary key on [Questions_IdQuestion], [Themes_ThemeId] in table 'LinkThemeQuestion'
ALTER TABLE [dbo].[LinkThemeQuestion]
ADD CONSTRAINT [PK_LinkThemeQuestion]
    PRIMARY KEY NONCLUSTERED ([Questions_IdQuestion], [Themes_ThemeId] ASC);
GO

-- Creating primary key on [Subjects_SubjectId], [Themes_ThemeId] in table 'LinkThemeSubject'
ALTER TABLE [dbo].[LinkThemeSubject]
ADD CONSTRAINT [PK_LinkThemeSubject]
    PRIMARY KEY NONCLUSTERED ([Subjects_SubjectId], [Themes_ThemeId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AccountId] in table 'LinkAccountTests'
ALTER TABLE [dbo].[LinkAccountTests]
ADD CONSTRAINT [FK_LinkAccountTest_Account]
    FOREIGN KEY ([AccountId])
    REFERENCES [dbo].[Accounts]
        ([AccountId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkAccountTest_Account'
CREATE INDEX [IX_FK_LinkAccountTest_Account]
ON [dbo].[LinkAccountTests]
    ([AccountId]);
GO

-- Creating foreign key on [WhoAssignedAccountId] in table 'LinkAccountTests'
ALTER TABLE [dbo].[LinkAccountTests]
ADD CONSTRAINT [FK_LinkAccountTest_Account1]
    FOREIGN KEY ([WhoAssignedAccountId])
    REFERENCES [dbo].[Accounts]
        ([AccountId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkAccountTest_Account1'
CREATE INDEX [IX_FK_LinkAccountTest_Account1]
ON [dbo].[LinkAccountTests]
    ([WhoAssignedAccountId]);
GO

-- Creating foreign key on [AccountId] in table 'TestResults'
ALTER TABLE [dbo].[TestResults]
ADD CONSTRAINT [FK_TestResult_Account]
    FOREIGN KEY ([AccountId])
    REFERENCES [dbo].[Accounts]
        ([AccountId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TestResult_Account'
CREATE INDEX [IX_FK_TestResult_Account]
ON [dbo].[TestResults]
    ([AccountId]);
GO

-- Creating foreign key on [AnswerId] in table 'LinkAnswerResults'
ALTER TABLE [dbo].[LinkAnswerResults]
ADD CONSTRAINT [FK_LinkAnswerResult_Answer]
    FOREIGN KEY ([AnswerId])
    REFERENCES [dbo].[Answers]
        ([AnswerId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkAnswerResult_Answer'
CREATE INDEX [IX_FK_LinkAnswerResult_Answer]
ON [dbo].[LinkAnswerResults]
    ([AnswerId]);
GO

-- Creating foreign key on [TestId] in table 'LinkAccountTests'
ALTER TABLE [dbo].[LinkAccountTests]
ADD CONSTRAINT [FK_LinkAccountTest_Test]
    FOREIGN KEY ([TestId])
    REFERENCES [dbo].[Tests]
        ([TestId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkAccountTest_Test'
CREATE INDEX [IX_FK_LinkAccountTest_Test]
ON [dbo].[LinkAccountTests]
    ([TestId]);
GO

-- Creating foreign key on [TestResultId] in table 'LinkAnswerResults'
ALTER TABLE [dbo].[LinkAnswerResults]
ADD CONSTRAINT [FK_AnswerToResult_TestResult]
    FOREIGN KEY ([TestResultId])
    REFERENCES [dbo].[TestResults]
        ([TestResultId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [SubjectId] in table 'Tests'
ALTER TABLE [dbo].[Tests]
ADD CONSTRAINT [FK_Test_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([SubjectId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Test_Subject'
CREATE INDEX [IX_FK_Test_Subject]
ON [dbo].[Tests]
    ([SubjectId]);
GO

-- Creating foreign key on [TestId] in table 'TestResults'
ALTER TABLE [dbo].[TestResults]
ADD CONSTRAINT [FK_TestResult_Test]
    FOREIGN KEY ([TestId])
    REFERENCES [dbo].[Tests]
        ([TestId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TestResult_Test'
CREATE INDEX [IX_FK_TestResult_Test]
ON [dbo].[TestResults]
    ([TestId]);
GO

-- Creating foreign key on [Accounts_AccountId] in table 'LinkAccountRole'
ALTER TABLE [dbo].[LinkAccountRole]
ADD CONSTRAINT [FK_LinkAccountRole_Account]
    FOREIGN KEY ([Accounts_AccountId])
    REFERENCES [dbo].[Accounts]
        ([AccountId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Roles_RoleId] in table 'LinkAccountRole'
ALTER TABLE [dbo].[LinkAccountRole]
ADD CONSTRAINT [FK_LinkAccountRole_Role]
    FOREIGN KEY ([Roles_RoleId])
    REFERENCES [dbo].[Roles]
        ([RoleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkAccountRole_Role'
CREATE INDEX [IX_FK_LinkAccountRole_Role]
ON [dbo].[LinkAccountRole]
    ([Roles_RoleId]);
GO

-- Creating foreign key on [Accounts_AccountId] in table 'LinkAccountSubject'
ALTER TABLE [dbo].[LinkAccountSubject]
ADD CONSTRAINT [FK_LinkAccountSubject_Account]
    FOREIGN KEY ([Accounts_AccountId])
    REFERENCES [dbo].[Accounts]
        ([AccountId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Subjects_SubjectId] in table 'LinkAccountSubject'
ALTER TABLE [dbo].[LinkAccountSubject]
ADD CONSTRAINT [FK_LinkAccountSubject_Subject]
    FOREIGN KEY ([Subjects_SubjectId])
    REFERENCES [dbo].[Subjects]
        ([SubjectId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkAccountSubject_Subject'
CREATE INDEX [IX_FK_LinkAccountSubject_Subject]
ON [dbo].[LinkAccountSubject]
    ([Subjects_SubjectId]);
GO

-- Creating foreign key on [Tests_TestId] in table 'LinkQuestionTest'
ALTER TABLE [dbo].[LinkQuestionTest]
ADD CONSTRAINT [FK_LinkQuestionTest_Test]
    FOREIGN KEY ([Tests_TestId])
    REFERENCES [dbo].[Tests]
        ([TestId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Questions_IdQuestion] in table 'LinkQuestionTest'
ALTER TABLE [dbo].[LinkQuestionTest]
ADD CONSTRAINT [FK_LinkQuestionTest_Question]
    FOREIGN KEY ([Questions_IdQuestion])
    REFERENCES [dbo].[Questions]
        ([IdQuestion])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkQuestionTest_Question'
CREATE INDEX [IX_FK_LinkQuestionTest_Question]
ON [dbo].[LinkQuestionTest]
    ([Questions_IdQuestion]);
GO

-- Creating foreign key on [Tests_TestId] in table 'LinkTestToTypeTest'
ALTER TABLE [dbo].[LinkTestToTypeTest]
ADD CONSTRAINT [FK_LinkTestToTypeTest_Test]
    FOREIGN KEY ([Tests_TestId])
    REFERENCES [dbo].[Tests]
        ([TestId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TypeTests_TypeTestId] in table 'LinkTestToTypeTest'
ALTER TABLE [dbo].[LinkTestToTypeTest]
ADD CONSTRAINT [FK_LinkTestToTypeTest_TypeTest]
    FOREIGN KEY ([TypeTests_TypeTestId])
    REFERENCES [dbo].[TypeTests]
        ([TypeTestId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkTestToTypeTest_TypeTest'
CREATE INDEX [IX_FK_LinkTestToTypeTest_TypeTest]
ON [dbo].[LinkTestToTypeTest]
    ([TypeTests_TypeTestId]);
GO

-- Creating foreign key on [Questions_IdQuestion] in table 'LinkThemeQuestion'
ALTER TABLE [dbo].[LinkThemeQuestion]
ADD CONSTRAINT [FK_LinkThemeQuestion_Question]
    FOREIGN KEY ([Questions_IdQuestion])
    REFERENCES [dbo].[Questions]
        ([IdQuestion])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Themes_ThemeId] in table 'LinkThemeQuestion'
ALTER TABLE [dbo].[LinkThemeQuestion]
ADD CONSTRAINT [FK_LinkThemeQuestion_Theme]
    FOREIGN KEY ([Themes_ThemeId])
    REFERENCES [dbo].[Themes]
        ([ThemeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkThemeQuestion_Theme'
CREATE INDEX [IX_FK_LinkThemeQuestion_Theme]
ON [dbo].[LinkThemeQuestion]
    ([Themes_ThemeId]);
GO

-- Creating foreign key on [Subjects_SubjectId] in table 'LinkThemeSubject'
ALTER TABLE [dbo].[LinkThemeSubject]
ADD CONSTRAINT [FK_LinkThemeSubject_Subject]
    FOREIGN KEY ([Subjects_SubjectId])
    REFERENCES [dbo].[Subjects]
        ([SubjectId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Themes_ThemeId] in table 'LinkThemeSubject'
ALTER TABLE [dbo].[LinkThemeSubject]
ADD CONSTRAINT [FK_LinkThemeSubject_Theme]
    FOREIGN KEY ([Themes_ThemeId])
    REFERENCES [dbo].[Themes]
        ([ThemeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LinkThemeSubject_Theme'
CREATE INDEX [IX_FK_LinkThemeSubject_Theme]
ON [dbo].[LinkThemeSubject]
    ([Themes_ThemeId]);
GO

-- Creating foreign key on [QuestionId] in table 'LinkAnswerResults'
ALTER TABLE [dbo].[LinkAnswerResults]
ADD CONSTRAINT [FK_QuestionLinkAnswerResult]
    FOREIGN KEY ([QuestionId])
    REFERENCES [dbo].[Questions]
        ([IdQuestion])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionLinkAnswerResult'
CREATE INDEX [IX_FK_QuestionLinkAnswerResult]
ON [dbo].[LinkAnswerResults]
    ([QuestionId]);
GO

-- Creating foreign key on [QuestionId] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [FK_QuestionAnswer]
    FOREIGN KEY ([QuestionId])
    REFERENCES [dbo].[Questions]
        ([IdQuestion])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionAnswer'
CREATE INDEX [IX_FK_QuestionAnswer]
ON [dbo].[Answers]
    ([QuestionId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------