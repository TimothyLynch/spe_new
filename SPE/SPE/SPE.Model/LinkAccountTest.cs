
//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------


namespace SPE.Model
{

using System;
    using System.Collections.Generic;
    
public partial class LinkAccountTest
{

    public int AssignedTestId { get; set; }

    public int TestId { get; set; }

    public int AccountId { get; set; }

    public System.DateTime AssignedDate { get; set; }

    public int WhoAssignedAccountId { get; set; }



    public virtual Account Account { get; set; }

    public virtual Account Account1 { get; set; }

    public virtual Test Test { get; set; }

}

}
