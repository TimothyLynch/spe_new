﻿using log4net;

namespace SPE.Model.Helpers
{
    public static class Logger
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Logger));

        private static void Configuration()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public static ILog Log
        {
            get
            {
                Configuration();
                return log;
            }
        }

        public static void Error(string message)
        {
            Log.Error(message);
        }

        public static void Info(string message)
        {
            Log.Info(message);
        }
    }
}