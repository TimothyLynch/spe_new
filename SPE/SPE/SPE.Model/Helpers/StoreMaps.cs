﻿using AutoMapper;
using SPE.Model.DataModel;

namespace SPE.Model.Helpers
{
    public static class StoreMaps
    {
        public static void AllMappers()
        {
            AccountMappers();
            AnswerMappers();
            LinkAccountTestMappers();
            LinkAnswerResultMappers();
            QuestionMappers();
            RoleMappers();
            SubjectMappers();
            TestMappers();
            TestResultMappers();
            ThemeMappers();
            TypeTestMappers();
        }


        public static void AccountMappers()
        {
            Mapper.CreateMap<AccountDataModel, Account>().ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Role))
                                                        .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => src.Subject))
                                                        .ForMember(dest => dest.TestResults, opt => opt.MapFrom(src => src.TestResults))
                                                        .ForMember(dest => dest.LinkAccountTests, opt => opt.MapFrom(src => src.LinkAccountTest))
                                                        .ForMember(dest => dest.LinkAccountTests1, opt => opt.MapFrom(src => src.LinkAccountTest_1));
            Mapper.CreateMap<Account, AccountDataModel>().ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Roles))
                                                        .ForMember(dest => dest.Subject, opt => opt.MapFrom(src => src.Subjects))
                                                        .ForMember(dest => dest.TestResults, opt => opt.MapFrom(src => src.TestResults))
                                                        .ForMember(dest => dest.LinkAccountTest, opt => opt.MapFrom(src => src.LinkAccountTests))
                                                        .ForMember(dest => dest.LinkAccountTest_1, opt => opt.MapFrom(src => src.LinkAccountTests1));
        }

        public static void RoleMappers()
        {
            Mapper.CreateMap<RoleDataModel, Role>().ForMember(dest => dest.Accounts, opt => opt.MapFrom(src => src.Account));
            Mapper.CreateMap<Role, RoleDataModel>().ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Accounts));
        }


        public static void SubjectMappers()
        {
            Mapper.CreateMap<SubjectDataModel, Subject>().ForMember(dest => dest.Accounts, opt => opt.MapFrom(src => src.Account))
                                                                         .ForMember(dest => dest.Tests, opt => opt.MapFrom(src => src.Test))
                                                                         .ForMember(dest => dest.Themes, opt => opt.MapFrom(src => src.Theme));
            Mapper.CreateMap<Subject, SubjectDataModel>().ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Accounts))
                                                                        .ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Tests))
                                                                        .ForMember(dest => dest.Theme, opt => opt.MapFrom(src => src.Themes));
        }

        public static void AnswerMappers()
        {
            Mapper.CreateMap<AnswerDataModel, Answer>().ForMember(dest => dest.LinkAnswerResults, opt => opt.MapFrom(src => src.LinkAnswerResult))
                                                       .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question));
            Mapper.CreateMap<Answer, AnswerDataModel>().ForMember(dest => dest.LinkAnswerResult, opt => opt.MapFrom(src => src.LinkAnswerResults))
                                                       .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question));
        }

        public static void LinkAccountTestMappers()
        {
            Mapper.CreateMap<LinkAccountTestDataModel, LinkAccountTest>().ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Account))
                                                                         .ForMember(dest => dest.Account1, opt => opt.MapFrom(src => src.Account_1))
                                                                         .ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Test));
            Mapper.CreateMap<LinkAccountTest, LinkAccountTestDataModel>().ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Account))
                                                                        .ForMember(dest => dest.Account_1, opt => opt.MapFrom(src => src.Account1))
                                                                        .ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Test));
        }

        public static void LinkAnswerResultMappers()
        {
            Mapper.CreateMap<LinkAnswerResultDataModel, LinkAnswerResult>().ForMember(dest => dest.TestResult, opt => opt.MapFrom(src => src.TestResults))
                                                                         .ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.Answer))
                                                                         .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question));
            Mapper.CreateMap<LinkAnswerResult, LinkAnswerResultDataModel>().ForMember(dest => dest.TestResults, opt => opt.MapFrom(src => src.TestResult))
                                                                        .ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.Answer))
                                                                        .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Question));
        }

        public static void QuestionMappers()
        {
            Mapper.CreateMap<QuestionDataModel, Question>().ForMember(dest => dest.Answers, opt => opt.MapFrom(src => src.Answer))
                                                                         .ForMember(dest => dest.LinkAnswerResults, opt => opt.MapFrom(src => src.LinkAnswerResult))
                                                                         .ForMember(dest => dest.Tests, opt => opt.MapFrom(src => src.Test))
                                                                         .ForMember(dest => dest.Themes, opt => opt.MapFrom(src => src.Theme));
            Mapper.CreateMap<Question, QuestionDataModel>().ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.Answers))
                                                                        .ForMember(dest => dest.LinkAnswerResult, opt => opt.MapFrom(src => src.LinkAnswerResults))
                                                                        .ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Tests))
                                                                        .ForMember(dest => dest.Theme, opt => opt.MapFrom(src => src.Themes));
        }


        public static void TestMappers()
        {
            Mapper.CreateMap<TestDataModel, Test>().ForMember(dest => dest.TestResult, opt => opt.MapFrom(src => src.TestResult))
                                                   .ForMember(dest => dest.Questions, opt => opt.MapFrom(src => src.Question))
                                                   .ForMember(dest => dest.LinkAccountTests, opt => opt.MapFrom(src => src.LinkAccountTest))
                                                   .ForMember(dest => dest.TypeTests, opt => opt.MapFrom(src => src.TypeTest))
                                                   .ForMember(dest => dest.Subject, opt => opt.MapFrom(src => src.Subject));
            Mapper.CreateMap<Test, TestDataModel>().ForMember(dest => dest.TestResult, opt => opt.MapFrom(src => src.TestResult))
                                                   .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Questions))
                                                   .ForMember(dest => dest.LinkAccountTest, opt => opt.MapFrom(src => src.LinkAccountTests))
                                                   .ForMember(dest => dest.TypeTest, opt => opt.MapFrom(src => src.TypeTests))
                                                   .ForMember(dest => dest.Subject, opt => opt.MapFrom(src => src.Subject));
        }

        public static void TestResultMappers()
        {
            Mapper.CreateMap<TestResultsDataModel, TestResult>().ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Account))
                                                                         .ForMember(dest => dest.LinkAnswerResults, opt => opt.MapFrom(src => src.LinkAnswerResult))
                                                                         .ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Test));
            Mapper.CreateMap<TestResult, TestResultsDataModel>().ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.Account))
                                                                        .ForMember(dest => dest.LinkAnswerResult, opt => opt.MapFrom(src => src.LinkAnswerResults))
                                                                        .ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Test));
        }

        public static void ThemeMappers()
        {
            Mapper.CreateMap<ThemeDataModel, Theme>().ForMember(dest => dest.Questions, opt => opt.MapFrom(src => src.Question))
                                                       .ForMember(dest => dest.Subjects, opt => opt.MapFrom(src => src.Subject));
            Mapper.CreateMap<Theme, ThemeDataModel>().ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Questions))
                                                       .ForMember(dest => dest.Subject, opt => opt.MapFrom(src => src.Subjects));
        }

        public static void TypeTestMappers()
        {
            Mapper.CreateMap<TypeTestDataModel, TypeTest>().ForMember(dest => dest.Tests, opt => opt.MapFrom(src => src.Test));
            Mapper.CreateMap<TypeTest, TypeTestDataModel>().ForMember(dest => dest.Test, opt => opt.MapFrom(src => src.Tests));
        }
    }
}