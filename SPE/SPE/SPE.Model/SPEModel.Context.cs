﻿

//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------


namespace SPE.Model
{

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


public partial class SPEEntities : DbContext
{
    public SPEEntities()
        : base("name=SPEEntities")
    {

    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        throw new UnintentionalCodeFirstException();
    }


    public DbSet<Account> Accounts { get; set; }

    public DbSet<Answer> Answers { get; set; }

    public DbSet<LinkAccountTest> LinkAccountTests { get; set; }

    public DbSet<LinkAnswerResult> LinkAnswerResults { get; set; }

    public DbSet<Question> Questions { get; set; }

    public DbSet<Role> Roles { get; set; }

    public DbSet<Subject> Subjects { get; set; }

    public DbSet<Test> Tests { get; set; }

    public DbSet<TestResult> TestResults { get; set; }

    public DbSet<Theme> Themes { get; set; }

    public DbSet<TypeTest> TypeTests { get; set; }

}

}

