﻿using System.Windows.Controls;
using SPE.ViewModel;
using SPE.Model.DataModel;

namespace SPE.UserControls
{
    /// <summary>
    /// Interaction logic for MainTeacherView.xaml
    /// </summary>
    public partial class MainTeacherView : UserControl
    {
        public MainTeacherView()
        {
            InitializeComponent();
        }

        private void tcTeacher_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl) 
            {
                if (tiStatisticTeacher != null)
                {
                    if (tiStatisticTeacher.IsSelected)
                    {
                        var teacherView = new TeacherStatisticView
                        {
                            DataContext = new StatisticTeacherViewModel((DataContext as AccountDataModel).AccountId)
                        };
                        tiStatisticTeacher.Content = teacherView;
                    }
                }
                
                 if (tiDBTeacher != null)
                {
                    if (tiDBTeacher.IsSelected)
                    {
                        var teacherView = new TeacherWorkWithKnowledgeView
                        {
                            DataContext = new TeacherWorkWithKnowledgeViewModel((DataContext as AccountDataModel).AccountId)
                        };
                        tiDBTeacher.Content = teacherView;
                    }
                }

                if (tiTestsTeacher != null)
                {
                    if (tiTestsTeacher.IsSelected)
                    {
                        var teacherView = new TeacherWorkWithTestsView
                        {
                            DataContext = new TeacherWorkWithTestsViewModel((DataContext as AccountDataModel).AccountId)
                        };
                        tiTestsTeacher.Content = teacherView;
                    }
                }

                if (tiAssignedTeacher != null)
                {
                    if (tiAssignedTeacher.IsSelected)
                    {
                        var teacherView = new TeacherAssignmentTestsView
                        {
                            DataContext = new TeacherAssignmentTestViewModel((DataContext as AccountDataModel).AccountId)
                        };
                        tiAssignedTeacher.Content = teacherView;
                    }
                }
            }    
        }
    }
}
