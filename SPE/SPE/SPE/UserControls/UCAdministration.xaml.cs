﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SPE.Model.DataModel;

namespace SPE.UserControls
{
    /// <summary>
    /// Interaction logic for UCAdministration.xaml
    /// </summary>
    public partial class UCAdministration : UserControl
    {
        public UCAdministration()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var source = dgAccount.ItemsSource as ObservableCollection<AccountDataModel>;
            if (source == null) return;

            if (source.Any(a => a.Role.Any(b => b.Moderation) == false)) btnNewUser.Visibility = Visibility.Visible;
            else btnNewUser.Visibility = Visibility.Hidden;
        }

        private void btnNewUser_Click(object sender, RoutedEventArgs e)
        {
            var source = dgAccount.ItemsSource as ObservableCollection<AccountDataModel>;
            if (source.Count == 0) return;

            var sortResult = ((IEnumerable<AccountDataModel>)dgAccount.ItemsSource).OrderBy(a => a, AccountDataModel.SortModeration());
            dgAccount.ItemsSource = new ObservableCollection<AccountDataModel>(sortResult);
            dgAccount.UpdateLayout();

            foreach (AccountDataModel item in dgAccount.ItemsSource)
            {
                var row = dgAccount.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
                if (item.Role.Any(a => a.Moderation == false))
                    row.Background = Brushes.Pink;
                else
                    row.Background = Brushes.White;
                if (item.Role.Count == 0)
                    row.Background = Brushes.Yellow;
            }
        }
    }
}