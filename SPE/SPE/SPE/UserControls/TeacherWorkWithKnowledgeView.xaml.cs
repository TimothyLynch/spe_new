﻿using System.Windows.Input;
using System.Windows.Controls;

namespace SPE.UserControls
{
    /// <summary>
    /// Interaction logic for TeacherWorkWithKnowledgeView.xaml
    /// </summary>
    public partial class TeacherWorkWithKnowledgeView : UserControl
    {
        public TeacherWorkWithKnowledgeView()
        {
            InitializeComponent();
        }

        private void dgSubject_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            tiThemes.IsEnabled = true;
            tiQuestions.IsEnabled = false;
            tiAnswers.IsEnabled = false;
            tbSubjectName.Clear();
            tbSubjectDesc.Clear();

            tiThemes.IsSelected = true;
        }

        private void dgTheme_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            tiQuestions.IsEnabled = true;
            tiAnswers.IsEnabled = false;
            tbThemeName.Clear();
            tbThemeDesc.Clear();

            tiQuestions.IsSelected = true;
        }

        private void dgQuestion_MouseDoubleClick_1(object sender, MouseButtonEventArgs e)
        {
            tiAnswers.IsEnabled = true;
            tbTextQuestion.Clear();
            tbDescQuestion.Clear();

            tiAnswers.IsSelected = true;
        }

        private void btnAddAnsw_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            tbAnswerText.Clear();
            tbAnswerDesc.Clear(); 
        }

        private void btnAddQuest_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            tbTextQuestion.Clear();
            tbDescQuestion.Clear();
        }

        private void btnAddTh_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            tbThemeName.Clear();
            tbThemeDesc.Clear();
        }

        private void btnAddSubj_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            tbSubjectName.Clear();
            tbSubjectDesc.Clear();
        }
    }
}
