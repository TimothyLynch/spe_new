﻿using System.Windows.Controls;
using SPE.Model.DataModel;
using SPE.ViewModel;

namespace SPE.UserControls
{
	/// <summary>
	/// Interaction logic for StudentView.xaml
	/// </summary>
    public partial class StudentView : UserControl
	{
		public StudentView()
		{
			InitializeComponent();
			startTestBtn.IsEnabled = false;
		}

		private void allTestsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(e.AddedItems.Count > 0)
				startTestBtn.IsEnabled = true;
		}

        private void assignedTestsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
                startTestBtn.IsEnabled = true;
        }
	}
}
