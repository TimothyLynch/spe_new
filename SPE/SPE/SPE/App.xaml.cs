﻿using System.Windows;
using SPE.View;
using SPE.ViewModel;

namespace SPE
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var authorization = new AutorizationView
            {
                DataContext = new AutorizationViewModel()
            };
            authorization.Show();
        }
    }
}