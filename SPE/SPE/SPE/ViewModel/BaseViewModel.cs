﻿using System.Threading.Tasks;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Model.Services;
using SPE.Command;
using System.Windows.Input;
using SPE.View;
using System.Linq;
using SPE.Helpers;
using SPE.Model.DataModel;
using SPE.Model.Services.Interface;
using SPE.Model.Repository.Common;

namespace SPE.ViewModel
{
    internal class BaseViewModel
    {
        #region Field

        private IAdministrationService _serviseAdmin;
        private IExpandedRepository<AccountDataModel> _providerAccount;
        private static readonly object Locker = new object();
        private readonly Task _loadDateAsync;
        private DelegateCommand<object> _clickCommand;

        #endregion Field

        #region Command

        public ICommand LogOutCommand
        {
            get
            {
                _clickCommand = new DelegateCommand<object>(LogOut);
                return _clickCommand;
            }
        }

        #endregion

        #region Constructor

        public BaseViewModel(AccountDataModel account)
        {
            Account = account;
            _loadDateAsync = Task.Factory.StartNew(LoadDateAsync);
        }

        #endregion Constructor

        #region Property

        public AccountDataModel Account { get; set; }

        #endregion Property

        #region Metod

        private void LoadDateAsync()
        {
            lock (Locker)
            {
                _providerAccount = new AccountRepositorySQL();
                _serviseAdmin = new AdministrationService(_providerAccount);
                _serviseAdmin.AccountIsActive(Account, true);
            }
        }

        private void LogOut(object obj)
        {
            var authorization = new AutorizationView
            {
                DataContext = new AutorizationViewModel()
            };
            authorization.Show();

            WindowsManager.Close(typeof (BaseView));

            if (_loadDateAsync.IsCompleted)
            {
                var account =
                    (AccountDataModel)
                        _providerAccount.Get(a => a.FirstOrDefault(b => b.AccountId == Account.AccountId));
                _serviseAdmin.AccountIsActiveAsync(account, false);
            }
        }

        #endregion Metod
    }
}