﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using SPE.Model.DataModel;
using System.Windows.Input;
using SPE.Model.Services;
using SPE.Command;
using System.Threading;
using System.Collections.ObjectModel;
using SPE.View.HelperView;

namespace SPE.ViewModel
{
    public class TeacherWorkWithTestsViewModel
    {
        #region Fields

        private DelegateCommand<TestDataModel> _clickAddTestCommand;

        private TeacherService _teacherService;

        private readonly static object Locker = new object();
        #endregion Fields

        #region Constructor

        public TeacherWorkWithTestsViewModel(int accountId)
        {
            AccountId = accountId;

            Test = new TestDataModel();

            Subjects = new List<SubjectDataModel>();
            TypeTests = new List<TypeTestDataModel>();

            Tests = new ObservableCollection<TestDataModel>();

            var thread = new Thread(ThreadLoadData);
            thread.Start();
        }

        #endregion Constructor

        #region Properties

        public ObservableCollection<TestDataModel> Tests { get; set; }

        public TestDataModel Test { get; set; }

        public List<SubjectDataModel> Subjects { get; set; }
        public List<TypeTestDataModel> TypeTests { get; set; }

        private int AccountId { get; set; }

        public TestDataModel SelectedTest
        {
            set
            {
                if (value == null) return;
                Test.TestName = value.TestName;
                Test.TestDesc = value.TestDesc;
                Test.TestIsEndEdit = value.TestIsEndEdit;
                Test.TestCountViewQuestion = value.TestCountViewQuestion;
                Test.TestSetTime = value.TestSetTime;
            }
            get { return Test; }
        }

        public int SelectTypeTest
        {
            set 
            {
                Test.TypeTest.Clear();
                Test.TypeTest.Add(TypeTests.FirstOrDefault(a => a.TypeTestId == value)); 
            }
        }

        #endregion Properties

        #region Command

        public ICommand AddTestCommand
        {
            get 
            {
                return _clickAddTestCommand ?? (_clickAddTestCommand = new DelegateCommand<TestDataModel>(AddTestClick));
            }
        }
        
        #endregion

        #region Methods

        private void ThreadLoadData()
        {
            lock (Locker)
            {               
                _teacherService = new TeacherService(AccountId);

                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    _teacherService.GetTests(AccountId).ToList().ForEach(a => Tests.Add(a));
                    _teacherService.GetSubjects().ToList().ForEach(a => Subjects.Add(a));
                    _teacherService.GetTypeTests().ToList().ForEach(a => TypeTests.Add(a));
                }));
            }
        }

        public void AddTestClick(TestDataModel test)
        {
            try
            {
                if (Test == null) return;
                _teacherService.AddTest(Test);
                Tests.Clear();
                _teacherService.GetTests(AccountId).ToList().ForEach(a => Tests.Add(a));
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }

        #endregion
    }
}

