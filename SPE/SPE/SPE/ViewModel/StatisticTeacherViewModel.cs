﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using SPE.Model.DataModel;
using System.Threading;
using SPE.Model.Repository.DataProvider.SQL;

namespace SPE.ViewModel
{
    class StatisticTeacherViewModel
    {
        private int AccountId { get; set; }

        public ObservableCollection<StudentStatisticModel> Results { get; set; }
        public List<SubjectDataModel> Subjects { get; set; }
        public List<AccountDataModel> Accounts { get; set; }
        private readonly static object Locker = new object();

        public StatisticTeacherViewModel(int accountId)
        {
            AccountId = accountId;
            Results = new ObservableCollection<StudentStatisticModel>();
            Subjects = new List<SubjectDataModel>();
            Accounts = new List<AccountDataModel>();
            var thread = new Thread(ThreadLoadData);
            thread.Start();
        }

        private int _selectedId;
        public int SelectSubject
        {
            get { return _selectedId; }
            set
            {
                _selectedId = value;
                if (SelectAccount > 0)
                {
                    Results.Clear();
                    LoadResultsTests();
                }
            }
        }

        private int _accountId;
        public int SelectAccount
        {
            get { return _accountId; }
            set
            {
                _accountId = value;
                if (SelectSubject > 0)
                {
                    Results.Clear();
                    LoadResultsTests();
                }
            }
        }

        private void ThreadLoadData()
        {
            lock (Locker)
            {
                LoadSubjects();
                LoadAccounts();
            }
        }

        private void LoadAccounts()
        {
            Accounts.AddRange(new AccountRepositorySQL().GetEntities());
        }

        private void LoadSubjects()
        {
            Subjects.AddRange(new SubjectRepositorySQL().GetEntities());
        }

        private void LoadResultsTests()
        {
            var _testProvider = new TestRepositorySQL();
            var results = _testProvider.GetResults(SelectAccount, SelectSubject);

            foreach (var item in results)
            {
                Results.Add(item);
            }
        }
    }
}
