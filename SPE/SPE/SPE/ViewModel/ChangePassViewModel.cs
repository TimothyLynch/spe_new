﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using SPE.Command;
using SPE.Helpers;
using SPE.Model.DataModel;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Model.Services;
using SPE.View;
using SPE.View.HelperView;

namespace SPE.ViewModel
{
    public class ChangePassViewModel
    {
        #region Fields

        private DelegateCommand<object> _cliclCommand;
        private readonly AdministrationService _adminService;
        private readonly AccountDataModel _account;

        #endregion Fields

        #region Constructor

        public ChangePassViewModel(AccountDataModel account)
        {
            _account = account;
            _adminService = new AdministrationService(new AccountRepositorySQL());
        }

        #endregion Constructor

        #region Command

        public ICommand SaveCommand
        {
            get { return _cliclCommand ?? (_cliclCommand = new DelegateCommand<object>(Save)); }
        }

        #endregion Command

        #region Metods

        public void Save(object obj)
        {
            var passwordControl = obj as PasswordBox;
            _account.Password = passwordControl.Password;

            if (string.IsNullOrWhiteSpace(_account.Password))
            {
                new SPEMessage(StateMessage.Error, RS_Message.NotField).Show();
                return;
            }
            try
            {
                _adminService.ChangePass(_account);
                WindowsManager.Close(typeof(ChangePassView));
                new SPEMessage(StateMessage.Message, RS_Message.PasswordIsChanged).Show();
            }
            catch (Exception ex)
            {
                new SPEMessage(StateMessage.Error, ex.Message).Show();
            }
        }

        #endregion Metods
    }
}