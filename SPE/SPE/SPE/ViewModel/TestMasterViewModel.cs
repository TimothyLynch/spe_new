﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPE.Command;
using SPE.Model.DataModel;
using SPE.Model.Services;
using System.Windows.Input;
using SPE.View;
using System.Windows.Threading;
using SPE.Helpers;
using SPE.Model.DataModel.Common;

namespace SPE.ViewModel
{
    public class TestMasterViewModel
    {
        public class TimerManager : BaseNotify
        {
            private TimeSpan _remainingTime;

            public TimeSpan RemainingTime
            {
                get { return _remainingTime; }
                set
                {
                    _remainingTime = value;
                    OnPropertyChanged("RemainingTime");
                }
            }
        }

        public class AnswerShow : BaseNotify
        {
            private string _print = String.Empty;

            public string Print
            {
                get { return _print; }
                set
                {
                    _print = value;
                    OnPropertyChanged("Print");
                }
            }
        }

        #region Fields

        private readonly TestingSevice _testingService = new TestingSevice();
        private DelegateCommand<object> _clickEndExam;
        private TimeSpan _timeForTest;
        private readonly DispatcherTimer _dispatcherTimer;

        #endregion

        #region Properties

        private int AccountId { get; set; }
        private int TestId { get; set; }
        public List<QuestionDataModel> Questions { get; set; }
        public List<AnswerDataModel> Answers { get; set; }
        public TimerManager Timer { get; set; }
        public AnswerShow TrueAnswer { get; set; }

        private QuestionDataModel _questionData = new QuestionDataModel();

        public QuestionDataModel SelectedQuestion
        {
            get { return _questionData; }
            set
            {
                _questionData = value;
                Answers.Clear();
                Answers.AddRange(_questionData.Answer);

                if (TrueAnswer == null) return;
                TrueAnswer.Print = String.Empty;
                int cntOfAnswer = 0;
                switch (value.TypeQuestion)
                {
                    case 1:
                        foreach (var item in value.Answer.Where(item => item.TypeAnswerValue == 1))
                        {
                            cntOfAnswer++;
                            TrueAnswer.Print += String.Format("\n#{0}. {1} потому, что: {2}.", cntOfAnswer, item.AnswerText, item.AnswerDesc);
                        }
                        break;
                    case 2:
                    {
                        foreach (var item in value.Answer.Where(item => item.TypeAnswerValue == 1))
                        {
                            cntOfAnswer++;
                            TrueAnswer.Print += String.Format("\n#{0}. {1} потому, что: {2}.", cntOfAnswer, item.AnswerText, item.AnswerDesc);
                        }
                    }
                        break;
                }
            }
        }

        #endregion

        #region Constructor

        public TestMasterViewModel(int accountId, int testId)
        {
            AccountId = accountId;
            TestId = testId;

            Questions = _testingService.GetQuestionFromTestRandom(testId).ToList();

            var cnt = 0;
            foreach (var item in Questions)
            {
                cnt++;
                item.DisplayName = String.Format("Question {0}", cnt);
            }

             Answers = new List<AnswerDataModel>();

            //create && binding true answer for education type test
            if (_testingService.GetTypeTestNameByTestId(testId) == "Education")
            {
                TrueAnswer = new AnswerShow();
            }

            //create && setting timer
            Timer = new TimerManager();
            _timeForTest =
                new TimeSpan((_testingService.GetTimeForTest(testId) != null)
                    ? _testingService.GetTimeForTest(testId).Value.Ticks
                    : 60);
            Timer.RemainingTime = _timeForTest;

            _dispatcherTimer = new DispatcherTimer {Interval = TimeSpan.FromSeconds(1)};
            _dispatcherTimer.Tick += (s, e) => Tick();

            _dispatcherTimer.IsEnabled = true;
        }

        //tick for timer
        private void Tick()
        {
            if ((int) Timer.RemainingTime.TotalSeconds < 1)
            {
                _dispatcherTimer.IsEnabled = false;
                return;
            }
            Timer.RemainingTime = Timer.RemainingTime.Subtract(TimeSpan.FromSeconds(1));
        }

        #endregion

        #region Methods

        public void EndExamClick(object obj)
        {
            _dispatcherTimer.IsEnabled = false;

            int trueQuestion = GetCountTrueQuesions();
            var cntAllQuestion = Questions.Count();

            var percent = ((double) trueQuestion/(double) cntAllQuestion)*100;

            var testResultDm = new TestResultsDataModel
            {
                AccountId = AccountId,
                TestId = TestId,
                TimeTest = _timeForTest.Subtract(Timer.RemainingTime),
                TimeToTest = _timeForTest,
                TypeTestId = _testingService.GetTypeTestIdByTestId(TestId),
                CountBadQuestion = cntAllQuestion - trueQuestion,
                StatusTest = 1,
                TestCountQuestion = cntAllQuestion,
                TestDateTime = DateTime.Now,
                TestResult1 = ((int) percent >= 80) ? true : false
            };

            _testingService.AddTestResult(testResultDm);

            //View model creates for print results after test
            var resultAfterTestVm = new ResultAfterTestViewModel(AccountId)
            {
                CountBadQuestions = cntAllQuestion - trueQuestion,
                CountQuestions = cntAllQuestion,
                HistoryOfAnswersOnQuestions = Questions,
                ProcentOfTrueAnswers = (int) percent,
                StatusTest = (((double) trueQuestion/(double) cntAllQuestion) >= 0.8) ? true : false,
                TestToTime = _timeForTest,
                TestTime = _timeForTest.Subtract(Timer.RemainingTime),
                IndexOfQuestion = 0
            };

            var baseView = new ResultAfterTestView
            {
                DataContext = resultAfterTestVm
            };
            baseView.Show();
            WindowsManager.Close(typeof (TestMasterView));
        }

        private int GetCountTrueQuesions()
        {
            int result = 0;
            foreach (var item in Questions)
            {
                switch (item.TypeQuestion)
                {
                    case 1:
                        result +=
                            item.Answer.Where(answ => answ.IsChecked)
                                .Count(answ => answ.TypeAnswerValue == Convert.ToInt32(answ.IsChecked));
                        break;
                    case 2:
                    {
                        int cntTrueAnswers = item.Answer.Where(a => a.TypeAnswerValue == 1).ToList().Count();
                        int cntIsChecked = item.Answer.Where(a => a.IsChecked == true).ToList().Count();

                        if (cntTrueAnswers != cntIsChecked) continue;
                        double koeff = 1.0/cntTrueAnswers;
                        double partOfQuestion =
                            (from answ in item.Answer
                                where answ.IsChecked
                                where answ.TypeAnswerValue == Convert.ToInt32(answ.IsChecked)
                                select koeff).Sum();
                        result += (partOfQuestion == 1) ? 1 : 0;
                    }
                        break;
                }
            }
            return result;
        }

        #endregion

        #region Commands

        public ICommand EndExam
        {
            get { return _clickEndExam ?? (_clickEndExam = new DelegateCommand<object>(EndExamClick)); }
        }

        #endregion
    }
}
