﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using SPE.Model.DataModel;
using System.Configuration;
using System.Globalization;

namespace SPE.ViewModel.Convertors
{
    public class BoolTeacheConvertor : IValueConverter
    {
        private object _buffer;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            _buffer = value;
            return value is ICollection<RoleDataModel> &&
                   RoleChaked.GetRole((ICollection<RoleDataModel>) value, RS_Message.Teacher, true);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (_buffer is ICollection<RoleDataModel>)
            {
                RoleChaked.SetRole((ICollection<RoleDataModel>)_buffer, (bool)value, RS_Message.Teacher, int.Parse(ConfigurationManager.AppSettings.Get(RS_Message.Teacher)));
                return _buffer;
            }
            return _buffer;
        }
    }
}