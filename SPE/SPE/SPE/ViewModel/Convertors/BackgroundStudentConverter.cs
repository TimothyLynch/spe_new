﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using SPE.Model.DataModel;

namespace SPE.ViewModel.Convertors
{
    public class BackgroundStudentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var roles = value as ICollection<RoleDataModel>;
            return roles != null ? RoleChaked.GetBackgroundRole(roles, RS_Message.Student, false) : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}