﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Data;
using SPE.Model.DataModel;
using System.Globalization;

namespace SPE.ViewModel.Convertors
{
    public class BoolAdminConvertor : IValueConverter
    {
        private object _buffer;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            _buffer = value;
            var roles = value as ICollection<RoleDataModel>;
            return roles != null && RoleChaked.GetRole(roles, RS_Message.Admin, true);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (_buffer is ICollection<RoleDataModel>)
            {
                RoleChaked.SetRole((ICollection<RoleDataModel>)_buffer, (bool)value, RS_Message.Admin, int.Parse(ConfigurationManager.AppSettings.Get(RS_Message.Admin)));
                return _buffer;
            }
            return _buffer;
        }
    }
}