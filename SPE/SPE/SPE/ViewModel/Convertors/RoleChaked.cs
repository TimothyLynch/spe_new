﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using SPE.Model.DataModel;

namespace SPE.ViewModel.Convertors
{
    public static class RoleChaked
    {
        public static void SetRole(ICollection<RoleDataModel> roles, bool value, string roleName, int roleId)
        {
            if (value)
            {
                if (roles.Any(a => !a.Moderation))
                {
                    roles.Remove(roles.FirstOrDefault(a => !a.Moderation));
                }
                roles.Add(new RoleDataModel { RoleId = roleId, Name = roleName, Moderation = true });
            }
            else
            {
                roles.Remove(roles.FirstOrDefault(a => a.RoleId == roleId));
            }
        }

        public static bool GetRole(ICollection<RoleDataModel> roles, string roleName, bool moderator)
        {
            return roles.Any(a => a.Name.Equals(roleName) && a.Moderation == moderator);
        }

        public static SolidColorBrush GetBackgroundRole(ICollection<RoleDataModel> roles, string roleName, bool moderator)
        {
            return GetRole(roles, roleName, moderator) ? Brushes.Bisque : null;
        }
    }
}