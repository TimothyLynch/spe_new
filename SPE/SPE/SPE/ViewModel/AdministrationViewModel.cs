﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using SPE.Command;
using SPE.Helpers;
using SPE.Model.DataModel;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Model.Services;
using SPE.View;
using SPE.View.HelperView;

namespace SPE.ViewModel
{
    public class AdministrationViewModel
    {
        #region Fields

        private readonly Dispatcher _dispatcher;
        private AccountRepositorySQL _providerAccount;
        private AdministrationService _serviceAdmin;
        private readonly static object Locker = new object();
        private DelegateCommand<object> _cliclCommand;
        private List<AccountDataModel> _checkChange;
        private readonly AccountDataModel _baseAccount;
        private Task taskSaveUpdate;

        #endregion Fields

        #region Constructor

        public AdministrationViewModel(AccountDataModel account)
        {
            _baseAccount = account;
            _checkChange = new List<AccountDataModel>();
            _dispatcher = Dispatcher.CurrentDispatcher;
            Accounts = new ObservableCollection<AccountDataModel>();
            Task.Factory.StartNew(LoadDataAsync, TaskCreationOptions.PreferFairness);
        }

        #endregion Constructor

        #region Properties

        public ObservableCollection<AccountDataModel> Accounts { get; set; }

        public AccountDataModel SelectAccount { get; set; }

        public List<string> FullName
        {
            get
            {
                var result = new List<string>();
                result.AddRange(Accounts.Select(a => string.Format(RS_Message.FullName, a.Name, a.MiddleName, a.LastName)));
                return result;
            }
        }

        #endregion Properties

        #region Command

        public ICommand RegistrationCommand
        {
            get
            {
                _cliclCommand = new DelegateCommand<object>(InsertClick);
                return _cliclCommand;
            }
        }

        public ICommand RemovingCommand
        {
            get
            {
                _cliclCommand = new DelegateCommand<object>(RemovClick);
                return _cliclCommand;
            }
        }

        public ICommand SaveAllCommand
        {
            get
            {
                _cliclCommand = new DelegateCommand<object>(SaveAllClick);
                return _cliclCommand;
            }
        }

        public ICommand ChangePassCommand
        {
            get
            {
                _cliclCommand = new DelegateCommand<object>(ChangePassClick);
                return _cliclCommand;
            }
        }

        #endregion Command

        #region Method

        public void InsertClick(object account)
        {
            var registration = new RegistrationView
            {
                DataContext = new RegistrationViewModel(StateInsertAccount.Insert)
            };

            if (registration.ShowDialog() != (bool?)false)
            {
                var temp = ((RegistrationViewModel)registration.DataContext).Account;

                if (temp != null)
                {
                    if (!Accounts.Any(a => temp.Login.Equals(a.Login)))
                    {
                        temp.Password = AdministrationService.GetHashPasswors(temp.Password);
                        Accounts.Add(temp);
                    }
                    else
                    {
                        new SPEMessage(StateMessage.Message, RS_Message.UserUsed).Show();
                        return;
                    }
                }
            }
            _cliclCommand = null;
        }

        public void RemovClick(object obj)
        {
            var account = SelectAccount;

            if (account == null)
            {
                new SPEMessage(StateMessage.Error, RS_Message.SelectAnItemToDelete).Show();
                return;
            }

            if (account.AccountId == _baseAccount.AccountId)
            {
                new SPEMessage(StateMessage.Error, RS_Message.YouCanNotRemoveYourself).Show();
                return;
            }

            var dialog = new SPEDialog(StateDialog.Delete, String.Format(RS_Message.WantToDeleteAX0X1X2, SelectAccount.Name, SelectAccount.MiddleName, SelectAccount.LastName));
            if (dialog.ShowDialog() == true)
            {
                Accounts.Remove(SelectAccount);
            }
        }

        public void SaveAllClick(object obj)
        {
            if (!CheckAccount(_checkChange, Accounts.ToList()))
            {
                SPEDialog dialog = new SPEDialog(StateDialog.Update, SPE.RS_Message.SaveChanges);
                if (dialog.ShowDialog() == true)
                {
                    var taskUpadate = _serviceAdmin.UpdateAllAccount(Accounts);
                    taskSaveUpdate = Task.Factory.StartNew(() =>
                    {
                        //taskUpadate.Wait();
                        UpdateAccountDispatcher();
                    });
                }
            }
        }

        public void ChangePassClick(object obj)
        {
            var view = new ChangePassView
            {
                DataContext = new ChangePassViewModel(SelectAccount)
            };
            view.Show();
        }

        #region Helpers Methods

        private void LoadDataAsync()
        {
            lock (Locker)
            {
                _providerAccount = new AccountRepositorySQL();
                _serviceAdmin = new AdministrationService(_providerAccount);
                UpdateAccountDispatcher();
            }
        }

        private void UpdateAccountDispatcher()
        {
            _dispatcher.Invoke(new Action(UpdateAccount));
        }

        private void UpdateAccount()
        {
            Accounts.Clear();
            _providerAccount.GetEntities().ToList().ForEach(a => Accounts.Add(a));

            _checkChange = Accounts.Select(a => new AccountDataModel
            {
                Birthday = a.Birthday,
                IsEnable = a.IsEnable,
                LastName = a.LastName,
                Login = a.Login,
                MiddleName = a.MiddleName,
                Name = a.Name,
                Role = a.Role.Select(b => b).ToList()
            }).ToList();
        }

        private static bool CheckAccount(List<AccountDataModel> old, List<AccountDataModel> other)
        {
            old.Sort(AccountDataModel.SortName());
            other.Sort(AccountDataModel.SortName());

            if (old.Count != other.Count) return false;
            for (int i = 0; i < old.Count(); i++)
            {
                if (!old[i].Equals(other[i])) return false;
            }
            return true;
        }

        #endregion Helpers Methods

        #endregion Method
    }
}