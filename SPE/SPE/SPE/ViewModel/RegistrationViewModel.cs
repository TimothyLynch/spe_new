﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using SPE.Command;
using SPE.Helpers;
using SPE.Model.DataModel;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Model.Services;
using SPE.Model.Services.Interface;
using SPE.View;
using SPE.Model.DataModel.Common;
using SPE.View.HelperView;

namespace SPE.ViewModel
{
    public class RegistrationViewModel : BaseNotify
    {
        #region Fields

        private DelegateCommand<object> _cliclCommand;
        private RoleRepositorySQL _providerRole;
        private AccountRepositorySQL _providerAccount;
        private IAdministrationService _administrationService;

        private static readonly object Locker = new object();
        private readonly StateInsertAccount _state;

        private bool? _isCheckedSate = false;

        #endregion Fields

        #region Constructor

        public RegistrationViewModel(StateInsertAccount state)
        {
            _state = state;
            Account = new AccountDataModel {Birthday = DateTime.Now};
            Roles = new List<RoleDataModel>();
            Task.Factory.StartNew(LoadDataAsync, TaskCreationOptions.PreferFairness);
        }

        #endregion Constructor

        #region Properties

        public AccountDataModel Account { get; set; }

        public List<RoleDataModel> Roles { get; set; }

        public RoleDataModel SelectRole
        {
            set { Account.Role.Add((RoleDataModel) value); }
        }

        public bool? IsCheckesState
        {
            get { return _isCheckedSate; }

            set
            {
                _isCheckedSate = value;
                OnPropertyChanged(RS_Message.IsCheckesState);
            }
        }

        #endregion Properties

        #region Command

        public ICommand RegistrationCommand
        {
            get
            {
                if (_state == StateInsertAccount.Registration)
                    _cliclCommand = new DelegateCommand<object>(RegistrationClick);
                else
                    _cliclCommand = new DelegateCommand<object>(InsertClick);
                return _cliclCommand;
            }
        }

        #endregion Command

        #region Metods

        private void LoadDataAsync()
        {
            lock (Locker)
            {
                _providerRole = new RoleRepositorySQL();
                _providerAccount = new AccountRepositorySQL();
                _administrationService = new AdministrationService(_providerAccount);
                LoadRoles();
            }
        }

        private void LoadRoles()
        {
            if (Roles.Count > 0) return;
            Roles.AddRange(_state == StateInsertAccount.Registration
                ? _providerRole.GetEntities().Where(a => a.Moderation == false)
                : _providerRole.GetEntities().Where(a => a.Moderation == true));
        }

        public void InsertClick(object account)
        {
            var passwordControl = account as PasswordBox;
            Account.Password = passwordControl.Password;

            if (!Validation())
            {
                new SPEMessage(StateMessage.Message, RS_Message.NotField).Show();
            }
            else IsCheckesState = true;
        }

        public void RegistrationClick(object account)
        {
            var passwordControl = account as PasswordBox;
            Account.Password = passwordControl.Password;

            if (!Validation())
            {
                new SPEMessage(StateMessage.Message, RS_Message.NotField).Show();
                return;
            }

            try
            {
                if (_administrationService.RegistrationAccount(Account))
                {
                    var baseView = new BaseView
                    {
                        DataContext =
                            new BaseViewModel(
                                _providerAccount.GetEntities().FirstOrDefault(a => a.Login == Account.Login))
                    };
                    baseView.Show();
                    new SPEMessage(StateMessage.Message,
                        string.Format(RS_Message.SaveUser, Account.Name, Account.MiddleName, Account.LastName)).Show();
                }
            }
            catch (Exception ex)
            {
                new SPEMessage(StateMessage.Error, ex.Message).Show();
            }
            finally
            {
                Close();
            }
        }

        private static void Close()
        {
            WindowsManager.Close(typeof (RegistrationView));
        }

        public bool Validation()
        {
            return !string.IsNullOrWhiteSpace(Account.Login) &&
                   !string.IsNullOrWhiteSpace(Account.Password) &&
                   !string.IsNullOrWhiteSpace(Account.Name) &&
                   !string.IsNullOrWhiteSpace(Account.MiddleName) &&
                   !string.IsNullOrWhiteSpace(Account.LastName) &&
                   Account.Role.Count != 0;
        }

        #endregion Metods
    }
}