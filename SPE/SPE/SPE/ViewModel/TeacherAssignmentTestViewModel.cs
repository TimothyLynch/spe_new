﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using SPE.Model.Services;
using SPE.Model.DataModel;
using System.Collections.ObjectModel;
using SPE.Command;
using System.Windows.Input;
using System.Threading;
using SPE.View.HelperView;

namespace SPE.ViewModel
{
    public class TeacherAssignmentTestViewModel
    {
       #region Constructor
        public TeacherAssignmentTestViewModel(int teacherId)
        {
            AccountId = teacherId;
            _teacherService = new TeacherService(teacherId);

            Tests = new List<TestDataModel>();
            Accounts = new List<AccountDataModel>();

            LinkAccountTest = new LinkAccountTestDataModel();
            LinkAccountTests = new ObservableCollection<LinkAccountTestDataModel>();

            var thread = new Thread(ThreadLoadData);
            thread.Start();
        }
        #endregion Constructor

        #region Properties
        //fields
        private int AccountId { get; set; }
        private readonly static object Locker = new object();
        private DelegateCommand<LinkAccountTestDataModel> _clickAddLatCommand;

        //service
        private readonly TeacherService _teacherService;

        //combobox Tests
        public List<TestDataModel> Tests { get; set; }

        //combobox Accounts
        public List<AccountDataModel> Accounts { get; set; }

        //datagrid LinkAccountTests
        public ObservableCollection<LinkAccountTestDataModel> LinkAccountTests { get; set; }
        public LinkAccountTestDataModel LinkAccountTest { get; set; }

        public LinkAccountTestDataModel SelectedLinkAccountTest 
        {
            get { return LinkAccountTest; }

            set
            {
                if (value != null)
                {
                    LinkAccountTest.AssignedTestId = value.AssignedTestId;
                    LinkAccountTest.TestId = value.TestId;
                    LinkAccountTest.AccountId = value.AccountId;
                    LinkAccountTest.AssignedDate = value.AssignedDate;
                }
            }
        }

        #endregion

        #region Command 
        public ICommand AddLinkAccountTestCommand
        {
            get {
                return _clickAddLatCommand ??
                       (_clickAddLatCommand = new DelegateCommand<LinkAccountTestDataModel>(AddLinkAccountTest));
            }
        }
        #endregion

        #region Methods
        private void ThreadLoadData()
        {
            lock (Locker)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    _teacherService.GetTests(AccountId).ToList().ForEach(a => Tests.Add(a));
                    _teacherService.GetLinkAccountTests().ToList().ForEach(a => LinkAccountTests.Add(a));
                    _teacherService.GetAccounts().ToList().ForEach(a => Accounts.Add(a));
                });
            }

        }

        public void AddLinkAccountTest(LinkAccountTestDataModel lat)
        {
            try
            {
                if (_teacherService.AddLinkAccountTest(LinkAccountTest))
                {
                    LinkAccountTests.Clear();

                    _teacherService.GetLinkAccountTests().ToList().ForEach(a => LinkAccountTests.Add(a));
                }
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }
        #endregion
    }
}
