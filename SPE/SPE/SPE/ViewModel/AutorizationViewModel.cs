﻿using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using SPE.Command;
using SPE.Helpers;
using SPE.Model.DataModel;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Model.Services;
using SPE.Model.Services.Interface;
using SPE.View;
using SPE.Model.Helpers;
using SPE.View.HelperView;

namespace SPE.ViewModel
{
    internal class AutorizationViewModel
    {
        #region Fields

        private DelegateCommand<object> _cliclCommand;
        private IAdministrationService _serviseAdmin;
        private readonly static object Locker = new object();

        #endregion Fields

        #region Constructor

        public AutorizationViewModel()
        {
            StoreMaps.AllMappers();
            Account = new AccountDataModel();
            Task.Factory.StartNew(LoadDataAsync, TaskCreationOptions.PreferFairness);
        }

        #endregion Constructor

        #region Properties

        public AccountDataModel Account { get; set; }

        #endregion Properties

        #region Command

        public ICommand LogInCommand
        {
            get
            {
                _cliclCommand = new DelegateCommand<object>(LogInClick);
                return _cliclCommand;
            }
        }

        public ICommand SingUpCommand
        {
            get
            {
                _cliclCommand = new DelegateCommand<object>(SingUpClick);
                return _cliclCommand;
            }
        }

        #endregion Command

        #region Metods

        private void LoadDataAsync()
        {
            lock (Locker)
            {
                _serviseAdmin = new AdministrationService(new AccountRepositorySQL());
            }
        }

        public void LogInClick(object account)
        {
            var passwordBox = account as PasswordBox;
            Account.Password = passwordBox.Password;

            if (string.IsNullOrWhiteSpace(Account.Login)
                && string.IsNullOrWhiteSpace(Account.Password))
            {
                new SPEMessage(StateMessage.Message, RS_Message.NotField).Show();
                return;
            }

            var user = _serviseAdmin.Authorization(Account.Login, Account.Password);
            if (user != null)
            {
                
                if (!user.IsEnable)
                {
                    new SPEMessage(StateMessage.Message, RS_Message.UserIsLocked).Show();
                    return;
                }                

                var baseView = new BaseView
                {
                    DataContext = new BaseViewModel(user)
                };
                baseView.Show();
                Close();
            }
            else new SPEMessage(StateMessage.Message, RS_Message.NotExist).Show();
        }

        public void SingUpClick(object account)
        {
            var registration = new RegistrationView
            {
                DataContext = new RegistrationViewModel(StateInsertAccount.Registration)
            };
            registration.ShowDialog();
            Close();
        }

        private static void Close()
        {
            WindowsManager.Close(typeof(AutorizationView));
        }

        #endregion Metods
    }
}