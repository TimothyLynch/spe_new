﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SPE.Model.DataModel;
using System.Windows.Input;
using SPE.Model.Repository.DataProvider.SQL;
using System.Windows.Forms;
using SPE.Model.Services;
using SPE.Command;
using System.Threading;
using SPE.View;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace SPE.ViewModel
{
    public class TestTeacherViewModel
    {
        #region Fields
        private DelegateCommand<List<SubjectDataModel>> clickAddSubjCommand;
        private DelegateCommand<ThemeDataModel> clickAddThemeCommand;
        private DelegateCommand<SubjectDataModel> clickSubjCommand;

        private AccountRepositorySQL providerAccount;
        private SubjectRepositorySQL providerSubject;
        private ThemeRepositorySQL providerTheme;

        private TeacherService teacherService;

        private readonly static object locker = new object();

        private int AccountId { get; set; }

        #endregion Fields

        #region Constructor

        public TestTeacherViewModel(int accountId)
        {
            Subject = new SubjectDataModel();
            Theme = new ThemeDataModel();

            Subjects = new ObservableCollection<SubjectDataModel>();
            Themes = new ObservableCollection<ThemeDataModel>();
            AccountId = accountId;

            Thread thread = new Thread(ThreadLoadData);
            thread.Start();
        }

        #endregion Constructor

        #region Properties

        public ObservableCollection<SubjectDataModel> Subjects { get; set; }
        public ObservableCollection<ThemeDataModel> Themes { get; set; }
        public SubjectDataModel SelectedSubject { get; set; }

        public SubjectDataModel Subject { get; set; }
        public ThemeDataModel Theme { get; set; }

        #endregion Properties

        #region Command

        public ICommand AddSubjectCommand
        {
            get
            {
                if (clickAddSubjCommand == null)
                {
                    clickAddSubjCommand = new DelegateCommand<List<SubjectDataModel>>(AddSubjectClick);
                }
                return clickAddSubjCommand;
            }

        }

        public ICommand ClickSubjCommand
        {
            get
            {
                if (clickSubjCommand == null)
                {
                    clickSubjCommand = new DelegateCommand<SubjectDataModel>(SubjectClick);
                }
                return clickSubjCommand;
            }
        }

        public ICommand AddThemeCommand
        {
            get
            {
                if (clickAddThemeCommand == null)
                {
                    clickAddThemeCommand = new DelegateCommand<ThemeDataModel>(AddThemeClick);
                }
                return clickAddThemeCommand;
            }

        }

        #endregion

        #region Metods

        private void ThreadLoadData()
        {
            lock (locker)
            {
                providerAccount = new AccountRepositorySQL();
                providerSubject = new SubjectRepositorySQL();
                providerTheme = new ThemeRepositorySQL();

                teacherService = new TeacherService(AccountId);

                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    teacherService.GetSubjects().ToList().ForEach(c => Subjects.Add(c));
                });

                
            }

        }

        public void AddSubjectClick(List<SubjectDataModel> subject)
        {
            try
            {
                if (teacherService.AddSubject(Subject))
                {
                    
                    Subjects.Clear();
                    
                    teacherService.GetSubjects().ToList().ForEach(c => Subjects.Add(c));
                }
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            };

        }

        public void AddThemeClick(ThemeDataModel theme)
        {
            try
            {
                
                if (teacherService.AddTheme(Theme, SelectedSubject.SubjectId))
                {
                    this.SubjectClick(new SubjectDataModel());
                }
                
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            };

        }

        public void SubjectClick(SubjectDataModel subject)
        {
            try
            {
                if (SelectedSubject != null)
                {
                    Themes.Clear();

                    teacherService.GetThemes(SelectedSubject.SubjectId).ToList().ForEach(d => Themes.Add(d));
                }
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            };

        }

        #endregion
    }
}
