﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPE.Model.DataModel;
using SPE.View;
using SPE.Command;
using System.Windows.Input;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Helpers;

namespace SPE.ViewModel
{
    public class ResultAfterTestViewModel
    {
        #region Constructor
        public ResultAfterTestViewModel(int accountId) 
        {
            AccountId = accountId;
        }
        #endregion

        #region Properties
        private int AccountId { get; set; }

        public List<QuestionDataModel> HistoryOfAnswersOnQuestions { get; set; }
        private DelegateCommand<object> _clickCloseTestResult;

        private int _index;
        public int IndexOfQuestion
        {
            get { return _index; }
            set 
            {
                if (value < HistoryOfAnswersOnQuestions.Count)
                {
                    if (value < 0)
                    {
                        _index = HistoryOfAnswersOnQuestions.Count-1;
                        SelectedQuestion.Answer.Clear();
                        foreach (var item in HistoryOfAnswersOnQuestions[_index].Answer)
                        {
                            SelectedQuestion.Answer.Add(item);
                        }
                        SelectedQuestion.IdQuestion = HistoryOfAnswersOnQuestions[_index].IdQuestion;
                        SelectedQuestion.TextQuestion = HistoryOfAnswersOnQuestions[_index].TextQuestion;
                        SelectedQuestion.TypeQuestion = HistoryOfAnswersOnQuestions[_index].TypeQuestion;
                    }
                    else
                    {
                        _index = value;
                        //SelectedQuestion = HistoryOfAnswersOnQuestions[_index];
                        SelectedQuestion.Answer.Clear();
                        foreach (var item in HistoryOfAnswersOnQuestions[_index].Answer)
                        {
                            SelectedQuestion.Answer.Add(item);
                        }
                        SelectedQuestion.IdQuestion = HistoryOfAnswersOnQuestions[_index].IdQuestion;
                        SelectedQuestion.TextQuestion = HistoryOfAnswersOnQuestions[_index].TextQuestion;
                        SelectedQuestion.TypeQuestion = HistoryOfAnswersOnQuestions[_index].TypeQuestion;
                    }
                }
                else
                {
                    _index = 0;
                    SelectedQuestion.Answer.Clear();
                    foreach (var item in HistoryOfAnswersOnQuestions[_index].Answer)
                    {
                        SelectedQuestion.Answer.Add(item);
                    }
                    SelectedQuestion.IdQuestion = HistoryOfAnswersOnQuestions[_index].IdQuestion;
                    SelectedQuestion.TextQuestion = HistoryOfAnswersOnQuestions[_index].TextQuestion;
                    SelectedQuestion.TypeQuestion = HistoryOfAnswersOnQuestions[_index].TypeQuestion;
                }
            }
        }

        private QuestionDataModel _selQuestion = new QuestionDataModel();
        public QuestionDataModel SelectedQuestion { get { return _selQuestion; } set { _selQuestion = value; } }

        public bool StatusTest { get; set; }
        public int ProcentOfTrueAnswers { get; set; }

        public TimeSpan TestToTime { get; set; }
        public TimeSpan TestTime { get; set; }
        public string PrintSpentTimeOnTest
        {
            get { return String.Format("{0} of {1}", TestTime, TestToTime); }
        }

        public int CountQuestions { get; set; }
        public int CountBadQuestions { get; set; }
        public string PrintCountTrueAnswers
        {
            get { return String.Format("{0} of {1}", (CountQuestions-CountBadQuestions), CountQuestions); }
        }

        public void ClickCloseTestResult(object obj)
        {
            var baseView = new BaseView
            {
                DataContext = new BaseViewModel(new AccountRepositorySQL().GetEntities().FirstOrDefault(a => a.AccountId == AccountId))
            };
            baseView.Show();
            WindowsManager.Close(typeof(ResultAfterTestView));
        }

        #endregion

        #region Commands
        public ICommand CloseTestResult
        {
            get {
                return _clickCloseTestResult ??
                       (_clickCloseTestResult = new DelegateCommand<object>(ClickCloseTestResult));
            }
        }
        #endregion
    }
}
