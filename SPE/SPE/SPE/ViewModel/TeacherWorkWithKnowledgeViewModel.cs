﻿using System;
using System.Linq;
using System.Windows;
using SPE.Model.DataModel;
using System.Windows.Input;
using SPE.Model.Services;
using SPE.Command;
using System.Threading;
using System.Collections.ObjectModel;
using SPE.View.HelperView;

namespace SPE.ViewModel
{
    class TeacherWorkWithKnowledgeViewModel
    {
        #region Fields
        private DelegateCommand<SubjectDataModel> _clickAddSubjCommand;
        private DelegateCommand<ThemeDataModel> _clickAddThemeCommand;
        private DelegateCommand<SubjectDataModel> _clickSubjCommand;
        private DelegateCommand<ThemeDataModel> _clickThCommand;
        private DelegateCommand<QuestionDataModel> _clickAddQuestionCommand;
        private DelegateCommand<QuestionDataModel> _clickQuestCommand;
        private DelegateCommand<AnswerDataModel> _clickAddAnswerCommand;

        private TeacherService _teacherService;

        private readonly static object Locker = new object();
        #endregion Fields

        #region Constructor

        public TeacherWorkWithKnowledgeViewModel(int accountId)
        {
            Subject = new SubjectDataModel();
            Theme = new ThemeDataModel();
            Question = new QuestionDataModel();
            Answer = new AnswerDataModel();

            Questions = new ObservableCollection<QuestionDataModel>();
            Subjects = new ObservableCollection<SubjectDataModel>();
            Themes = new ObservableCollection<ThemeDataModel>();
            Answers = new ObservableCollection<AnswerDataModel>();

            AccountId = accountId;
            var thread = new Thread(ThreadLoadData);
            thread.Start();
        }

        #endregion Constructor

        #region Properties

        public ObservableCollection<SubjectDataModel> Subjects { get; set; }
        public ObservableCollection<ThemeDataModel> Themes { get; set; }
        public ObservableCollection<QuestionDataModel> Questions { get; set; }
        public ObservableCollection<AnswerDataModel> Answers { get; set; }

        public SubjectDataModel Subject { get; set; }
        public ThemeDataModel Theme { get; set; }
        public QuestionDataModel Question { get; set; }
        public AnswerDataModel Answer { get; set; }

        private int AccountId { get; set; }

        public SubjectDataModel SelectedSubject
        {
            set
            {
                if (value == null) return;
                Subject.SubjectId = value.SubjectId;
                Subject.SubjectName = value.SubjectName;
                Subject.SubjectDesc = value.SubjectDesc;
            }
            get { return Subject; }
        }

        public ThemeDataModel SelectedTheme
        {
            set
            {
                if (value == null) return;
                Theme.ThemeId = value.ThemeId;
                Theme.ThemeName = value.ThemeName;
                Theme.ThemeDesc = value.ThemeDesc;
            }

            get { return Theme; }
        }

        public QuestionDataModel SelectedQuestion
        {
            set
            {
                if (value == null) return;
                Question.IdQuestion = value.IdQuestion;
                Question.TextQuestion = value.TextQuestion;
                Question.DescQuestion = value.DescQuestion;
                Question.TypeQuestion = value.TypeQuestion;
            }
            get { return Question; }
        }

        public AnswerDataModel SelectedAnswer
        {
            set
            {
                if (value == null) return;
                Answer.AnswerId = value.AnswerId;
                Answer.AnswerText = value.AnswerText;
                Answer.AnswerDesc = value.AnswerDesc;
                Answer.TypeAnswerValue = value.TypeAnswerValue;
            }
            get { return Answer; }
        }

        #endregion Properties

        #region Command

        public ICommand AddSubjectCommand
        {
            get 
            {
                return _clickAddSubjCommand ??
                       (_clickAddSubjCommand = new DelegateCommand<SubjectDataModel>(AddSubjectClick));
            }
        }

        public ICommand AddThemeCommand
        {
            get 
            {
                return _clickAddThemeCommand ??
                       (_clickAddThemeCommand = new DelegateCommand<ThemeDataModel>(AddThemeClick));
            }
        }

        public ICommand AddQuestionCommand
        {
            get 
            {
                return _clickAddQuestionCommand ??
                       (_clickAddQuestionCommand = new DelegateCommand<QuestionDataModel>(AddQuestionClick));
            }
        }

        public ICommand AddAnswerCommand
        {
            get 
            {
                return _clickAddAnswerCommand ??
                       (_clickAddAnswerCommand = new DelegateCommand<AnswerDataModel>(AddAnswerClick));
            }
        }

        public ICommand ClickSubjCommand
        {
            get 
            {
                return _clickSubjCommand ?? (_clickSubjCommand = new DelegateCommand<SubjectDataModel>(SubjectClick));
            }
        }       

        public ICommand ClickThCommand
        {
            get { return _clickThCommand ?? (_clickThCommand = new DelegateCommand<ThemeDataModel>(ThemeClick)); }
        }

        public ICommand ClickQuestCommand
        {
            get 
            {
                return _clickQuestCommand ??
                       (_clickQuestCommand = new DelegateCommand<QuestionDataModel>(QuestionClick));
            }
        }

        #endregion

        #region Methods

        private void ThreadLoadData()
        {
            lock (Locker)
            {               
                _teacherService = new TeacherService(AccountId);

                Application.Current.Dispatcher.Invoke((Action)(() =>
                    _teacherService.GetSubjects().ToList().ForEach(c => Subjects.Add(c))));
            }

        }

        public void AddSubjectClick(SubjectDataModel subject)
        {
            try
            {
                if (Subject.SubjectDesc == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "SubjectDesc")).Show();
                    Subject.SubjectDesc = String.Empty;
                    Subject.SubjectName = String.Empty;
                    return;
                }
                if (Subject.SubjectName == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "SubjectName")).Show();
                    Subject.SubjectDesc = String.Empty;
                    Subject.SubjectName = String.Empty;
                    return;
                }

                if (_teacherService.AddSubject(Subject))
                {
                    Subjects.Clear();
                    _teacherService.GetSubjects().ToList().ForEach(c => Subjects.Add(c));
                }
                Subject.SubjectDesc = String.Empty;
                Subject.SubjectName = String.Empty;
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }

        public void AddThemeClick(ThemeDataModel theme)
        {
            try
            {
                if (Theme.ThemeName == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "ThemeName")).Show();
                    Theme.ThemeName = String.Empty;
                    Theme.ThemeDesc = String.Empty;
                    return;
                }
                if (Theme.ThemeDesc == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "ThemeDesc")).Show();
                    Theme.ThemeName = String.Empty;
                    Theme.ThemeDesc = String.Empty;
                    return;
                }

                if (_teacherService.AddTheme(Theme, Subject.SubjectId))
                {
                    SubjectClick(new SubjectDataModel());
                }
                Theme.ThemeName = String.Empty;
                Theme.ThemeDesc = String.Empty;
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }

        public void AddQuestionClick(QuestionDataModel question)
        {
            try
            {
                if (Question.TextQuestion == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "TextQuestion")).Show();
                    Question.TextQuestion = String.Empty;
                    Question.DescQuestion = String.Empty;
                    return;
                }
                if (Question.DescQuestion == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "DescQuestion")).Show();
                    Question.TextQuestion = String.Empty;
                    Question.DescQuestion = String.Empty;
                    return;
                }

                if (_teacherService.AddQuestion(Question, Theme.ThemeId))
                {
                    ThemeClick(new ThemeDataModel());
                }
                Question.TextQuestion = String.Empty;
                Question.DescQuestion = String.Empty;
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }

        public void AddAnswerClick(AnswerDataModel answer)
        {
            try
            {
                if (Answer.AnswerText == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "AnswerText")).Show();
                    Answer.AnswerText = String.Empty;
                    Answer.AnswerDesc = String.Empty;
                    return;
                }
                if (Answer.AnswerDesc == String.Empty)
                {
                    new SPEMessage(string.Format(RS_Message.EmptyTextBox, "AnswerDesc")).Show();
                    Answer.AnswerText = String.Empty;
                    Answer.AnswerDesc = String.Empty;
                    return;
                }

                if (_teacherService.AddAnswer(Answer, Question.IdQuestion, Question.TypeQuestion))
                {
                    QuestionClick(new QuestionDataModel());
                }
                Answer.AnswerText = String.Empty;
                Answer.AnswerDesc = String.Empty;
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }

        public void SubjectClick(SubjectDataModel subject)
        {
            try
            {
                if (Subject != null)
                {
                    Themes.Clear();
                    _teacherService.GetThemes(Subject.SubjectId).ToList().ForEach(d => Themes.Add(d));
                }
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }

        public void ThemeClick(ThemeDataModel theme)
        {
            try
            {
                if (Theme != null)
                {
                    Questions.Clear();
                    _teacherService.GetQuestions(Theme.ThemeId).ToList().ForEach(d => Questions.Add(d));
                }
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }

        public void QuestionClick(QuestionDataModel quesion)
        {
            try
            {
                if (Question != null)
                {
                    Answers.Clear();
                    _teacherService.GetAnswers(Question.IdQuestion).ToList().ForEach(d => Answers.Add(d));
                }
            }
            catch (Exception ex)
            {
                new SPEMessage(ex.Message).Show();
            }
        }
        #endregion
    }
}
