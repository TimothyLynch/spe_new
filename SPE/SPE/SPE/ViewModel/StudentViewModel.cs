﻿using System.Windows;
using System.Threading;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using SPE.View;
using SPE.Command;
using SPE.Model.DataModel;
using SPE.Model.Repository.DataProvider.SQL;
using SPE.Helpers;

namespace SPE.ViewModel
{
	public class StudentViewModel
	{
		#region Fields

		private SubjectRepositorySQL _subjectsProvider;
		private TestRepositorySQL _testProvider;
        private DelegateCommand<object> _clickCommand;
		private readonly static object Locker = new object();

		#endregion Fields

		public List<SubjectDataModel> Subjects { get; set; }
		public ObservableCollection<AssignedTestsModel> AssignedTests { get; set; }
		public ObservableCollection<AssignedTestsModel> AllTests { get; set; }
		public ObservableCollection<StudentStatisticModel> Results { get; set; }
        private int AccountId { get; set; }

		private int _selectedId;
		public int SelectSubject 
		{
			get { return _selectedId; }
			set 
			{
				_selectedId = value;

				AssignedTests.Clear();
				AllTests.Clear();
				Results.Clear();

				LoadAssignedTests();
				LoadAllTests();
				LoadResultsTests();
			}
		}

	    private AssignedTestsModel _testId;

	    public AssignedTestsModel SelectedTest
	    {
	        get { return _testId; }
	        set { _testId = value; }
	    }


	    public StudentViewModel(int accountId)
		{
            AccountId = accountId;
			Subjects = new List<SubjectDataModel>();
			AssignedTests = new ObservableCollection<AssignedTestsModel>();
			AllTests = new ObservableCollection<AssignedTestsModel>();
			Results = new ObservableCollection<StudentStatisticModel>();

			var thread = new Thread(ThreadLoadData);
			thread.Start();
		}

		#region Commands

		public ICommand StartTestCommand
		{
			get
			{
				_clickCommand = new DelegateCommand<object>(StartTestClick);
				return _clickCommand;
			}
		}


		#endregion Commands

		#region Methods

		private void ThreadLoadData()
		{
			lock (Locker)
			{
				LoadSubjects();
			}
		}

	    private void LoadSubjects()
		{
			_subjectsProvider = new SubjectRepositorySQL();
			Subjects.AddRange(_subjectsProvider.GetEntities());
		}

	    private void LoadAssignedTests()
		{
			_testProvider = new TestRepositorySQL();
            var results = _testProvider.GetAssignedTests(AccountId, SelectSubject);
			foreach (var item in results)
			{
				AssignedTests.Add(item);
			}
		}

	    private void LoadAllTests()
		{
			_testProvider = new TestRepositorySQL();
            var results = _testProvider.GetAllTests(SelectSubject);
			foreach (var item in results)
			{
				AllTests.Add(item);
			}
		}

	    private void LoadResultsTests()
		{
			_testProvider = new TestRepositorySQL();
            var results = _testProvider.GetResults(AccountId, SelectSubject);

			foreach (var item in results)
			{
				Results.Add(item);
			}
		}

		public void StartTestClick(object someObj)
		{
		    var testMasterView = new TestMasterView
		    {
		        DataContext = new TestMasterViewModel(AccountId, SelectedTest.TestId),
		    };

            if (SelectedTest.TestType == "Education")
            {
                testMasterView.btnShowAnswer.Visibility = Visibility.Visible;
            }
            testMasterView.Show();
            WindowsManager.Close(typeof(BaseView));
		}
		#endregion Methods
	}		
}
