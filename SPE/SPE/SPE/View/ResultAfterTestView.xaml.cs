﻿using System.Windows;
using SPE.ViewModel;

namespace SPE.View
{
    /// <summary>
    /// Interaction logic for ResultAfterTestView.xaml
    /// </summary>
    public partial class ResultAfterTestView : Window
    {
        public ResultAfterTestView()
        {
            InitializeComponent();
        }

        private void HistoryOfTest(object sender, RoutedEventArgs e)
        {
            if (gridHistory.Visibility == Visibility.Collapsed)
            {
                wnd.WindowState = System.Windows.WindowState.Maximized;
                wnd.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
                gridHistory.Visibility = Visibility.Visible;
                btnNextOfQuestion.Visibility = Visibility.Visible;
                btnPreviousOfQuestion.Visibility = Visibility.Visible;
            }
            else if (gridHistory.Visibility == Visibility.Visible)
            {
                WindowState = System.Windows.WindowState.Normal;
                wnd.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
                gridHistory.Visibility = Visibility.Collapsed;
                btnNextOfQuestion.Visibility = Visibility.Collapsed;
                btnPreviousOfQuestion.Visibility = Visibility.Collapsed;
            }
        }

        private void NextOfQuestion(object sender, RoutedEventArgs e)
        {
            var resultAfterTestViewModel = DataContext as ResultAfterTestViewModel;
            if (resultAfterTestViewModel != null)
                resultAfterTestViewModel.IndexOfQuestion++;
            ShowAnswers(DataContext as ResultAfterTestViewModel);
        }

        private void PreviousOfQuestion(object sender, RoutedEventArgs e)
        {
            var resultAfterTestViewModel = DataContext as ResultAfterTestViewModel;
            if (resultAfterTestViewModel != null)
                resultAfterTestViewModel.IndexOfQuestion--;
            ShowAnswers(DataContext as ResultAfterTestViewModel);
        }

        private void ShowAnswers(ResultAfterTestViewModel dataContext)
        {
            if (dataContext.SelectedQuestion.TypeQuestion == 1)
            {
                lbAnswersCheckBox.Visibility = Visibility.Hidden;
                lbAnswersRadio.Visibility = Visibility.Visible;
                lbAnswersRadio.Items.Refresh();
            }
            else if (dataContext.SelectedQuestion.TypeQuestion == 2)
            {
                lbAnswersRadio.Visibility = Visibility.Hidden;
                lbAnswersCheckBox.Visibility = Visibility.Visible;
                lbAnswersCheckBox.Items.Refresh();
            }
        }
    }
}
