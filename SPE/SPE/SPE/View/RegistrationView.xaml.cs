﻿using System.Windows;

namespace SPE.View
{
    /// <summary>
    /// Interaction logic for RegistrationView.xaml
    /// </summary>
    public partial class RegistrationView : Elysium.Controls.Window
    {
        public RegistrationView()
        {
            InitializeComponent();
            cbDialogResult.IsChecked = false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void tbPasswordRepeat_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tbPassword.Password != tbPasswordRepeat.Password)
                tbPasswordRepeat.Clear();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
        }

        private void cbDialogResult_Checked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}