﻿using System.Windows;

namespace SPE.View
{
    /// <summary>
    /// Interaction logic for ChangePass.xaml
    /// </summary>
    public partial class ChangePassView : Elysium.Controls.Window
    {
        public ChangePassView()
        {
            InitializeComponent();
        }

        private void tbPasswordRepeat_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tbPassword.Password != tbPasswordRepeat.Password)
                tbPasswordRepeat.Clear();
        }
    }
}