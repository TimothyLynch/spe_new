﻿using System.Windows;
using SPE.Helpers;

namespace SPE.View.HelperView
{
    /// <summary>
    /// Interaction logic for SPEDialog.xaml
    /// </summary>
    public partial class SPEDialog : Elysium.Controls.Window
    {
        public SPEDialog(StateDialog state, string message)
        {
            InitializeComponent();

            headerText.Text = state.ToString();
            subHeaderText.Text = message;
        }

        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}