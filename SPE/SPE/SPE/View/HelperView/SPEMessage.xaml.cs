﻿using SPE.Helpers;

namespace SPE.View.HelperView
{
    /// <summary>
    /// Interaction logic for Message.xaml
    /// </summary>
    public partial class SPEMessage : Elysium.Controls.Window
    {
        public SPEMessage(StateMessage state, string messge)
        {
            InitializeComponent();

            headerText.Text = state.ToString();
            subHeaderText.Text = messge;
        }

        public SPEMessage(string messge)
        {

            InitializeComponent();
            subHeaderText.Text = messge;
        }
    }
}