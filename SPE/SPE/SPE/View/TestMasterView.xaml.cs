﻿using System.Windows;
using System.Windows.Controls;
using SPE.Model.DataModel;

namespace SPE.View
{
	/// <summary>
	/// Interaction logic for TestMasterView.xaml
	/// </summary>
    public partial class TestMasterView : Elysium.Controls.Window
    {
        public TestMasterView()
        {
            InitializeComponent();
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lbQuestions.SelectedIndex = 0;
        }

        private void Next(object sender, RoutedEventArgs e)
        {
            if (lbQuestions.SelectedIndex < lbQuestions.Items.Count - 1)
                lbQuestions.SelectedIndex++;
        }

        private void Previous(object sender, RoutedEventArgs e)
        {
            if (lbQuestions.SelectedIndex > 0) lbQuestions.SelectedIndex--;
        }

        private void GenericTypeAnswers(object sender, SelectionChangedEventArgs args)
        {
            var questionDataModel = lbQuestions.SelectedItem as QuestionDataModel;
            if (questionDataModel != null && questionDataModel.TypeQuestion == 1)
            {
                lbAnswersCheckBox.Visibility = Visibility.Hidden;
                lbAnswersRadio.Visibility = Visibility.Visible;
                lbAnswersRadio.Items.Refresh();
            }
            var dataModel = lbQuestions.SelectedItem as QuestionDataModel;
            if (dataModel != null && dataModel.TypeQuestion == 2)
            {
                lbAnswersRadio.Visibility = Visibility.Hidden;
                lbAnswersCheckBox.Visibility = Visibility.Visible;
                lbAnswersCheckBox.Items.Refresh();
            }
        }

        private void btnShowAnswer_Click(object sender, RoutedEventArgs e)
        {
            var commandButton = sender as Elysium.Controls.CommandButton;
            if (commandButton == null) return;
            var btnContent = commandButton.Content.ToString();
            if (btnContent == "Show Answer")
            {
                tbTrueAnswer.Visibility = Visibility.Visible;
                commandButton.Content = "Hidden Answer";
            }
            else
            {
                tbTrueAnswer.Visibility = Visibility.Hidden;
                commandButton.Content = "Show Answer";
            }
        }
    }
}
