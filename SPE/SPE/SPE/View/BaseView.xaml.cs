﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using SPE.Model.DataModel;
using SPE.UserControls;
using SPE.ViewModel;

namespace SPE.View
{
    /// <summary>
    /// Interaction logic for BaseView.xaml
    /// </summary>
    public partial class BaseView : Elysium.Controls.Window
    {
        private int _numItem;

        public BaseView()
        {
            InitializeComponent();
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //var account = (e.NewValue as BaseViewModel).Account;

            //CreateUC(account);

            var source = e.NewValue as BaseViewModel;

            var account = source.Account;

            if (account.Role.Any(a => a.Moderation))
            {
                _numItem = 0;

                if (ChackRole(account, RS_Message.Admin))
                {
                    CreateChildrenElement(account, SPE.RS_Message.Admin, tabAdmin, gridAdmin, new UCAdministration() { DataContext = new AdministrationViewModel(account) });
                }
                if (ChackRole(account, SPE.RS_Message.Student))
                {
                    CreateChildrenElement(account, SPE.RS_Message.Student, tabStudent, gridStudent, new StudentView { DataContext = new StudentViewModel(account.AccountId) });
                }
                if (ChackRole(account, SPE.RS_Message.Teacher))
                {
                    CreateChildrenElement(account, SPE.RS_Message.Teacher, tabTeacher, gridTeacher, new MainTeacherView() { DataContext = account });
                }

            }
            else
            {
                CreateChildrenElement(account, SPE.RS_Message.Student, tabStudent, gridStudent, new StudentView { DataContext = new StudentViewModel(account.AccountId) });
            }
        }

        private void CreateChildrenElement(AccountDataModel account, string nameRole, TabItem itemControl, Grid gridRole, UserControl childrenRole)
        {
            itemControl.Visibility = Visibility.Visible;
            itemControl.IsEnabled = true;
            itemControl.IsSelected = true;

            tcMenu.Items.Remove(itemControl);
            tcMenu.Items.Insert(_numItem++, itemControl);

            if (childrenRole != null)
                gridRole.Children.Add(childrenRole);
        }

        //private void CreateChildrenElement(TabItem itemControl, Grid gridRole, UserControl childrenRole)
        //{
        //    itemControl.Visibility = Visibility.Visible;
        //    itemControl.IsEnabled = true;
        //    if (childrenRole != null)
        //        gridRole.Children.Add(childrenRole);
        //}

        private bool ChackRole(AccountDataModel account, string roleName)
        {
            return account.Role.Any(a => a.Name.Equals(roleName));
        }

        //private void CreateUC(AccountDataModel account)
        //{
        //    if (!account.IsEnable) return;

        //    if (CheckRole(account, RS_Message.Student))
        //    {
        //        CreateChildrenElement(tabStudent, gridStudent,
        //            new StudentView { DataContext = new StudentViewModel(account.AccountId) });
        //        tabStudent.IsSelected = true;
        //    }
        //    if (CheckRole(account, RS_Message.Teacher))
        //    {
        //        CreateChildrenElement(tabTeacher, gridTeacher,
        //            new MainTeacherView { DataContext = account });
        //        tabTeacher.IsSelected = true;
        //    }
        //    if (CheckRole(account, RS_Message.Admin))
        //    {
        //        CreateChildrenElement(tabAdmin, gridAdmin,
        //            new UCAdministration { DataContext = new AdministrationViewModel(account) });
        //        tabAdmin.IsSelected = true;
        //    }
        //}
    
    }
}