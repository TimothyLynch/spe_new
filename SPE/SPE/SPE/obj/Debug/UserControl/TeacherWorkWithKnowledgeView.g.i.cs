﻿#pragma checksum "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7CC7D7F79331841F35526E2B41545AF0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.261
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Elysium.Controls;
using Elysium.Parameters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SPE.UserControls {
    
    
    /// <summary>
    /// TeacherWorkWithKnowledgeView
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class TeacherWorkWithKnowledgeView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tcMain;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tiSubjects;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgSubject;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSubjectName;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSubjectDesc;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbSubjectName;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbSubjectDesc;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton btnAdd;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tiThemes;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgTheme;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblThemeName;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblThemeDesc;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbThemeName;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbThemeDesc;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton button1;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tiQuestions;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgQuestion;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTextQuestion;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDescQuestion;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTypeQuestion;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbTextQuestion;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbDescQuestion;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cb;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton button2;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tiAnswers;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGrid1;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbltbAnswerText;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAnswerDesc;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTypeAnswer;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbAnswerText;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbAnswerDesc;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbTrueAnswer;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton button3;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SPE;component/usercontrol/teacherworkwithknowledgeview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tcMain = ((System.Windows.Controls.TabControl)(target));
            return;
            case 2:
            this.tiSubjects = ((System.Windows.Controls.TabItem)(target));
            return;
            case 3:
            this.dgSubject = ((System.Windows.Controls.DataGrid)(target));
            
            #line 20 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
            this.dgSubject.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.dgSubject_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lblSubjectName = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.lblSubjectDesc = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.tbSubjectName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.tbSubjectDesc = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.btnAdd = ((Elysium.Controls.CommandButton)(target));
            return;
            case 9:
            this.tiThemes = ((System.Windows.Controls.TabItem)(target));
            return;
            case 10:
            this.dgTheme = ((System.Windows.Controls.DataGrid)(target));
            
            #line 49 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
            this.dgTheme.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.dgTheme_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 11:
            this.lblThemeName = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lblThemeDesc = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.tbThemeName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.tbThemeDesc = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.button1 = ((Elysium.Controls.CommandButton)(target));
            return;
            case 16:
            this.tiQuestions = ((System.Windows.Controls.TabItem)(target));
            return;
            case 17:
            this.dgQuestion = ((System.Windows.Controls.DataGrid)(target));
            
            #line 78 "..\..\..\UserControl\TeacherWorkWithKnowledgeView.xaml"
            this.dgQuestion.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.dgQuestion_MouseDoubleClick_1);
            
            #line default
            #line hidden
            return;
            case 18:
            this.lblTextQuestion = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.lblDescQuestion = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.lblTypeQuestion = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.tbTextQuestion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.tbDescQuestion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.cb = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 24:
            this.button2 = ((Elysium.Controls.CommandButton)(target));
            return;
            case 25:
            this.tiAnswers = ((System.Windows.Controls.TabItem)(target));
            return;
            case 26:
            this.dataGrid1 = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 27:
            this.lbltbAnswerText = ((System.Windows.Controls.Label)(target));
            return;
            case 28:
            this.lblAnswerDesc = ((System.Windows.Controls.Label)(target));
            return;
            case 29:
            this.lblTypeAnswer = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.tbAnswerText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.tbAnswerDesc = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.cbTrueAnswer = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 33:
            this.button3 = ((Elysium.Controls.CommandButton)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

