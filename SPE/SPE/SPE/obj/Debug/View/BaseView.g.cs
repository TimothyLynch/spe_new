﻿#pragma checksum "..\..\..\View\BaseView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7C67817C5FD115D81DCEA02AC724BE99"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.19462
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using Elysium.Controls;
using Elysium.Parameters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SPE.View {
    
    
    /// <summary>
    /// BaseView
    /// </summary>
    public partial class BaseView : Elysium.Controls.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\View\BaseView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tcMenu;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\View\BaseView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tabStudent;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\View\BaseView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridStudent;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\View\BaseView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tabTeacher;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\View\BaseView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridTeacher;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\View\BaseView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tabAdmin;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\View\BaseView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridAdmin;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SPE;component/view/baseview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\BaseView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\..\View\BaseView.xaml"
            ((SPE.View.BaseView)(target)).DataContextChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.Window_DataContextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tcMenu = ((System.Windows.Controls.TabControl)(target));
            return;
            case 3:
            this.tabStudent = ((System.Windows.Controls.TabItem)(target));
            return;
            case 4:
            this.gridStudent = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.tabTeacher = ((System.Windows.Controls.TabItem)(target));
            return;
            case 6:
            this.gridTeacher = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.tabAdmin = ((System.Windows.Controls.TabItem)(target));
            return;
            case 8:
            this.gridAdmin = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

