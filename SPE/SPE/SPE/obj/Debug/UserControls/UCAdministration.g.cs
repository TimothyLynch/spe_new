﻿#pragma checksum "..\..\..\UserControls\UCAdministration.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "BE2738CE1B2CD16B1D808A951D153D1C"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.19462
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using Elysium.Controls;
using Elysium.Parameters;
using SPE.ViewModel.Convertors;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SPE.UserControls {
    
    
    /// <summary>
    /// UCAdministration
    /// </summary>
    public partial class UCAdministration : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid baseGrid;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgAccount;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblName;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbName;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblMiddleName;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbMiddleName;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblLastName;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbLastName;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBirthday;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpBirthday;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblLogin;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbLogin;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblRole;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbRolesHidden;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton btnAdd;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton btnDel;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton btnOk;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblStud;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.ToggleSwitch tsStudent;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblThech;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.ToggleSwitch tsTeacher;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAdmin;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.ToggleSwitch tsAdmin;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblEnable;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.ToggleSwitch tsEnable;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPass;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Elysium.Controls.CommandButton btnChangePass;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\UserControls\UCAdministration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewUser;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SPE;component/usercontrols/ucadministration.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\UCAdministration.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\UserControls\UCAdministration.xaml"
            ((SPE.UserControls.UCAdministration)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.baseGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.dgAccount = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 4:
            this.lblName = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.tbName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.lblMiddleName = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.tbMiddleName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.lblLastName = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.tbLastName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.lblBirthday = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.dpBirthday = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 12:
            this.lblLogin = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.tbLogin = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.lblRole = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.cbRolesHidden = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.btnAdd = ((Elysium.Controls.CommandButton)(target));
            return;
            case 17:
            this.btnDel = ((Elysium.Controls.CommandButton)(target));
            return;
            case 18:
            this.btnOk = ((Elysium.Controls.CommandButton)(target));
            return;
            case 19:
            this.lblStud = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.tsStudent = ((Elysium.Controls.ToggleSwitch)(target));
            
            #line 71 "..\..\..\UserControls\UCAdministration.xaml"
            this.tsStudent.Checked += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            
            #line 71 "..\..\..\UserControls\UCAdministration.xaml"
            this.tsStudent.Unchecked += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 21:
            this.lblThech = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.tsTeacher = ((Elysium.Controls.ToggleSwitch)(target));
            
            #line 73 "..\..\..\UserControls\UCAdministration.xaml"
            this.tsTeacher.Checked += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            
            #line 73 "..\..\..\UserControls\UCAdministration.xaml"
            this.tsTeacher.Unchecked += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 23:
            this.lblAdmin = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.tsAdmin = ((Elysium.Controls.ToggleSwitch)(target));
            
            #line 75 "..\..\..\UserControls\UCAdministration.xaml"
            this.tsAdmin.Checked += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            
            #line 75 "..\..\..\UserControls\UCAdministration.xaml"
            this.tsAdmin.Unchecked += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 25:
            this.lblEnable = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.tsEnable = ((Elysium.Controls.ToggleSwitch)(target));
            return;
            case 27:
            this.lblPass = ((System.Windows.Controls.Label)(target));
            return;
            case 28:
            this.btnChangePass = ((Elysium.Controls.CommandButton)(target));
            return;
            case 29:
            this.btnNewUser = ((System.Windows.Controls.Button)(target));
            
            #line 81 "..\..\..\UserControls\UCAdministration.xaml"
            this.btnNewUser.Click += new System.Windows.RoutedEventHandler(this.btnNewUser_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

