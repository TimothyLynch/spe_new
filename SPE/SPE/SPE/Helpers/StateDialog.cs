﻿namespace SPE.Helpers
{
    public enum StateDialog
    {
        Delete = 0,
        Insert = 1,
        Update = 2
    }
}