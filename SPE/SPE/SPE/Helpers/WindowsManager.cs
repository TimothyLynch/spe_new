﻿using System;
using System.Threading.Tasks;
using System.Windows;

namespace SPE.Helpers
{
    public static class WindowsManager
    {
        private static WindowCollection CurrentWindows
        {
            get
            {
                return Application.Current.Windows;
            }
        }

        public static void Close(Type type)
        {
            foreach (Window item in CurrentWindows)
            {
                if (item.GetType() == type)
                {
                    item.Close();
                    return;
                }
            }
        }

        public static Task CloseAsync(Type type)
        {
            return Task.Factory.StartNew(() => Close(type));
        }
    }
}